﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WhereToBuy;

namespace WhereToBuyTester
{
    class Program
    {
        static void Main(string[] args)
        {

            List<string> excludedSuppliersFromOrder = ExcludedSuppliers.GetExcludedSupplierNames();

			CheapestPrice whereToBuy = new CheapestPrice();
			List<string> eans = new List<string>();
			eans.Add("4010168019116");
			//eans.Add("9783100022509");
			//eans.Add("5099996602225");
			//eans.Add("9783862293100");
			//eans.Add("3417761158548");
			//eans.Add("9783800178407");
			//eans.Add("9780199607952");
			//eans.Add("9783453237995");
			//eans.Add("9783905931587");
			//eans.Add("9781591394372");
			//eans.Add("5414233187196");
			//eans.Add("4002051692209");
			//eans.Add("4005556005581");
			//eans.Add("4002051692322");
			//eans.Add("5414233187196");

			foreach (var item in eans)
			{
				try
				{
					var cheapestPrice = whereToBuy.GetCheapestPriceByEan(item, "");
					Console.WriteLine(item.ToString() + " found.");
				}
				catch
				{
					Console.WriteLine(item.ToString() + " not found.");
				}
			}
			
            Console.Read();
        }
    }
}
