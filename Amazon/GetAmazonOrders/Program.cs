﻿using System;
using System.Collections.Generic;
using MarketplaceWebServiceOrders;
using System.IO;
using System.Text;
using InsertOrderXmlToMongoDb;
using System.Threading;
using DN_Classes.AppStatus;
using DN_Classes.Agent;
using System.Globalization;
using System.Linq;

namespace GetAmazonOrders
{
	public class Program
	{
		static String _accessKeyId = "";
		static String _secretAccessKey = "";
		static String _merchantId = "";
		static String _marketplaceId = "";
		private static readonly List<string> MarketplaceIdList = new List<string>();

		private static void Main()
		{
			var agent = new Agent();

			do
			{

				GetOrdersFromAmazon(agent);


				if (Properties.Settings.Default.Loop)
				{
					Thread.Sleep(TimeSpan.FromMinutes(10));
				}

			} while (Properties.Settings.Default.Loop);

		}

		private static void InsertOrdersToDb(string xml, string agentIDfromAmazon)
		{
			var inserter = new Inserter(xml);
			inserter.Insert(agentIDfromAmazon);
		}



		private static void GetOrdersFromAmazon(Agent agent)
		{

			if (agent != null)
			{

				using (ApplicationStatus appStatus = new ApplicationStatus("GetAmazonOrders " + agent.agentEntity.agent.agentname + " " + agent.agentEntity.agent.agentID.ToString()))
				{
					try
					{
						
						#region
						_accessKeyId = agent.agentEntity.agent.keys.amazon.accesskeyid;
						_secretAccessKey = agent.agentEntity.agent.keys.amazon.secretaccesskeyid;
						_merchantId = agent.agentEntity.agent.keys.amazon.merchantid;
						_marketplaceId = agent.agentEntity.agent.keys.amazon.marketplaceid;
                        MarketplaceIdList.Add(_marketplaceId);
                        MarketplaceIdList.AddRange(File.ReadLines("AdditionalMarketPlacesID.txt").ToList().Select( x => x.Split('\t')[0]).ToList());
                      
						const string applicationName = "a";
						const string applicationVersion = "0.00";


						var config = new MarketplaceWebServiceOrdersConfig
						{
							ServiceURL = "https://mws-eu.amazonservices.com/Orders/2011-01-01"
						};


						var service = new MarketplaceWebServiceOrdersClient(applicationName, applicationVersion, _accessKeyId, _secretAccessKey, config);

                        #region commented out
                        //var lateHour = TimeSpan.FromHours(2);

						//var stepSize = TimeSpan.FromHours(2);

						//var endDate = DateTime.Now.AddMinutes(-2);


						//int DaysBack = DateTime.Now.Hour == 2 ? GetAmazonOrders.Properties.Settings.Default.DaysBack : 1;

						//var startDate = DateTime.Now.AddDays(DaysBack * -1);

						//if (DateTime.Now.Hour == lateHour.Hours && DateTime.Now.Minute < 10)
						//{
						//	startDate = DateTime.Now.AddDays(10 * -1);
                        //}
                        #endregion

						var listTimeSteps = CreateTimeSteps(agent.agentEntity.agent.agentname, Properties.Settings.Default.OldDaysBack, Properties.Settings.Default.NewDaysBack);


						foreach (var timeSteps in listTimeSteps)
						{
							Console.WriteLine("PROCESSING ORDERS FROM {0} TO {1}", timeSteps.First().Key.ToString("dd.MM.yyy"), timeSteps.Last().Key.ToString("dd.MM.yyyy"));

							//ParallelOptions options = new ParallelOptions();
							//options.MaxDegreeOfParallelism = 2;

							//Parallel.ForEach(marketplaceIdList, options, id =>
							//{
							//	lock (new object())
							//	{
							foreach (var id in MarketplaceIdList)
							{
								foreach (var timeStep in timeSteps)
								{
									var start = timeStep.Key;
									var end = timeStep.Value;

									Console.WriteLine("{0} - Getting orders from {1} to {2}", id, start, end);
									var sb = new StringBuilder();
									sb.AppendLine("<OrderList>");
									sb.AppendLine(OrderFetcherSample.InvokeOrderFetcherSample(service, _merchantId, new[] { id }, start, end));
									sb.AppendLine("</OrderList>");
									InsertOrdersToDb(sb.ToString(), agent.agentEntity.agent.agentID.ToString());
									Thread.Sleep(TimeSpan.FromSeconds(Properties.Settings.Default.SleepingTime));
								}
								appStatus.Successful();
							} 
						}
                        
                               
                            
							//}
						//}
						//);
                        

						#endregion

                        Console.WriteLine("done");
						appStatus.AddMessagLine("Success " + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture));
						
					}
					catch (Exception ex)
					{
						appStatus.AddMessagLine(ex.Message);
					}
				}
			}

		}

		private static List<Dictionary<DateTime, DateTime>> CreateTimeSteps(string agentName, int oldDaysBack, int newDaysBack)
		{
			List<Dictionary<DateTime, DateTime>> createdTimeSteps = new List<Dictionary<DateTime, DateTime>>();
			var lateHour2Am = TimeSpan.FromHours(2);
			var lateHour3am = TimeSpan.FromHours(3);

			DateTime startDate = DateTime.Now.AddDays(newDaysBack * -1);
			createdTimeSteps.Add(CreateTimeSteps(TimeSpan.FromHours(2), DateTime.Now.AddMinutes(-2), startDate));

			startDate = DateTime.Now.AddDays(oldDaysBack * -1);
			createdTimeSteps.Add(CreateTimeSteps(TimeSpan.FromHours(2), DateTime.Now.AddMinutes(-2), startDate));

            #region commented out


            //DateTime startDate = DateTime.Now.AddDays(GetAmazonOrders.Properties.Settings.Default.DaysBack * -1);

            //if ((DateTime.Now.Hour == lateHour2am.Hours || DateTime.Now.Hour == lateHour3am.Hours) && DateTime.Now.Minute < 10)
            //{
            //    startDate = DateTime.Now.AddDays(10 * -1);
            //}
            //else
            //{

            //    CalculatedProfitDB cpdb = new CalculatedProfitDB();

            //    try
            //    {
            //        Console.WriteLine("Get PurchasedDate By Agent..");
            //        startDate = cpdb.collection.Find(Query.And(
            //                                                          Query.GTE("PurchasedDate", DateTime.Now.AddDays(-1)),
            //                                                          Query.EQ("Agentname", AgentName)
            //                                                  ))
            //                                  .SetFields("PurchasedDate")
            //                                      .Select(x => x.PurchasedDate)
            //                                          .OrderByDescending(y => y).ToList().First();
            //    }
            //    catch
            //    {

            //        Console.WriteLine("Failed to get date... Default date:" + GetAmazonOrders.Properties.Settings.Default.DaysBack);
            //    }

            //}
#endregion

			

			return createdTimeSteps;
		}

		private static Dictionary<DateTime, DateTime> CreateTimeSteps(TimeSpan hours, DateTime endDate, DateTime startDate)
		{
			var timeSteps = new Dictionary<DateTime, DateTime>();
			while (endDate > startDate)
			{
				var fromDate = startDate;
				var toDate = startDate + hours;

				if (toDate > DateTime.Now)
				{
					toDate = DateTime.Now;
				}
				timeSteps.Add(fromDate, toDate);

				startDate = toDate;
			}

			return timeSteps;
		}
	}
}
