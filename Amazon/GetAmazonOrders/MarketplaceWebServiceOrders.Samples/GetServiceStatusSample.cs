/******************************************************************************* 
 *  Copyright 2008-2012 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *  Licensed under the Apache License, Version 2.0 (the "License"); 
 *  
 *  You may not use this file except in compliance with the License. 
 *  You may obtain a copy of the License at: http://aws.amazon.com/apache2.0
 *  This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
 *  CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 *  specific language governing permissions and limitations under the License.
 * ***************************************************************************** 
 * 
 *  Marketplace Web Service Orders CSharp Library
 *  API Version: 2011-01-01
 * 
 */


using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using MarketplaceWebServiceOrders;
using MarketplaceWebServiceOrders.Model;
using System.IO;



namespace MarketplaceWebServiceOrders.Samples
{

    /// <summary>
    /// Get Service Status  Samples
    /// </summary>
    public class GetServiceStatusSample
    {
    
                                             
        /// <summary>
        /// Returns the service status of a particular MWS API section. The operation
        /// takes no input.
        /// 
        /// </summary>
        /// <param name="service">Instance of MarketplaceWebServiceOrders service</param>
        /// <param name="request">GetServiceStatusRequest request</param>
        public static void InvokeGetServiceStatus(MarketplaceWebServiceOrders service, GetServiceStatusRequest request)
        {
            string path = @"C:\Users\blesslargosa\Desktop\WriteText.txt";
            try 
            {
                GetServiceStatusResponse response = service.GetServiceStatus(request);
                
                
                Console.WriteLine ("Service Response");
                Console.WriteLine ("=============================================================================");
                Console.WriteLine ();

                using (StreamWriter sw = File.AppendText(path))
                {
                    sw.WriteLine("        GetServiceStatusResponse");
              
                }
                Console.WriteLine("        GetServiceStatusResponse");
                if (response.IsSetGetServiceStatusResult())
                {
                    using (StreamWriter sw = File.AppendText(path))
                    {
                        sw.WriteLine("            GetServiceStatusResult");
                   
                    }
                    Console.WriteLine("            GetServiceStatusResult");
                    GetServiceStatusResult  getServiceStatusResult = response.GetServiceStatusResult;
                    if (getServiceStatusResult.IsSetStatus())
                    {
                        using (StreamWriter sw = File.AppendText(path))
                        {
                            sw.WriteLine("                Status");
                            sw.WriteLine("                    {0}", getServiceStatusResult.Status);
                 
                        }
                        Console.WriteLine("                Status");
                        Console.WriteLine("                    {0}", getServiceStatusResult.Status);
                    }
                    if (getServiceStatusResult.IsSetTimestamp())
                    {
                        using (StreamWriter sw = File.AppendText(path))
                        {
                            sw.WriteLine("                Timestamp");
                            sw.WriteLine("                    {0}", getServiceStatusResult.Timestamp);
                
                        }
                        Console.WriteLine("                Timestamp");
                        Console.WriteLine("                    {0}", getServiceStatusResult.Timestamp);
                    }
                    if (getServiceStatusResult.IsSetMessageId())
                    {
                        using (StreamWriter sw = File.AppendText(path))
                        {
                            sw.WriteLine("                MessageId");
                            sw.WriteLine("                    {0}", getServiceStatusResult.MessageId);
                
                        }
                        Console.WriteLine("                MessageId");
                        Console.WriteLine("                    {0}", getServiceStatusResult.MessageId);
                    }
                    if (getServiceStatusResult.IsSetMessages())
                    {
                        using (StreamWriter sw = File.AppendText(path))
                        {
                            sw.WriteLine("                Messages");
                      
                        }
                        Console.WriteLine("                Messages");
                        MessageList  messages = getServiceStatusResult.Messages;
                        List<Message> messageList = messages.Message;
                        foreach (Message message in messageList)
                        {
                            Console.WriteLine("                    Message");
                            if (message.IsSetLocale())
                            {
                                using (StreamWriter sw = File.AppendText(path))
                                {
                                   sw.WriteLine("                        Locale");
                                    sw.WriteLine("                            {0}", message.Locale);
                          
                                }
                                Console.WriteLine("                        Locale");
                                Console.WriteLine("                            {0}", message.Locale);
                            }
                            if (message.IsSetText())
                            {
                                using (StreamWriter sw = File.AppendText(path))
                                {
                                    sw.WriteLine("                        Text");
                                   sw.WriteLine("                            {0}", message.Text);
                         
                                }
                                Console.WriteLine("                        Text");
                                Console.WriteLine("                            {0}", message.Text);
                            }
                        }
                    }
                }
                if (response.IsSetResponseMetadata())
                {
                    using (StreamWriter sw = File.AppendText(path))
                    {
                        sw.WriteLine("            ResponseMetadata");
                  
                    }
                    Console.WriteLine("            ResponseMetadata");
                    ResponseMetadata  responseMetadata = response.ResponseMetadata;
                    if (responseMetadata.IsSetRequestId())
                    {
                        using (StreamWriter sw = File.AppendText(path))
                        {
                            sw.WriteLine("                RequestId");
                            sw.WriteLine("                    {0}", responseMetadata.RequestId);
                
                        }
                        Console.WriteLine("                RequestId");
                        Console.WriteLine("                    {0}", responseMetadata.RequestId);
                    }
                }

                using (StreamWriter sw = File.AppendText(path))
                {
                    sw.WriteLine("            ResponseHeaderMetadata");
                    sw.WriteLine("                RequestId");
                    sw.WriteLine("                    " + response.ResponseHeaderMetadata.RequestId);
                    sw.WriteLine("                ResponseContext");
                    sw.WriteLine("                    " + response.ResponseHeaderMetadata.ResponseContext);
                    sw.WriteLine("                Timestamp");
                    sw.WriteLine("                    " + response.ResponseHeaderMetadata.Timestamp);
                    sw.WriteLine();

                }
                Console.WriteLine("            ResponseHeaderMetadata");
                Console.WriteLine("                RequestId");
                Console.WriteLine("                    " + response.ResponseHeaderMetadata.RequestId);
                Console.WriteLine("                ResponseContext");
                Console.WriteLine("                    " + response.ResponseHeaderMetadata.ResponseContext);
                Console.WriteLine("                Timestamp");
                Console.WriteLine("                    " + response.ResponseHeaderMetadata.Timestamp);
                Console.WriteLine();


            } 
            catch (MarketplaceWebServiceOrdersException ex) 
            {
                Console.WriteLine("Caught Exception: " + ex.Message);
                Console.WriteLine("Response Status Code: " + ex.StatusCode);
                Console.WriteLine("Error Code: " + ex.ErrorCode);
                Console.WriteLine("Error Type: " + ex.ErrorType);
                Console.WriteLine("Request ID: " + ex.RequestId);
                Console.WriteLine("XML: " + ex.XML);
                Console.WriteLine("ResponseHeaderMetadata: " + ex.ResponseHeaderMetadata);
            }
        }
        }
}
