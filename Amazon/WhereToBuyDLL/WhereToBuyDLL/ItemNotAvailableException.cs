﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WhereToBuyDLL
{
    [Serializable]
    public class ItemNotAvailableException : Exception
    {
        public ItemNotAvailableException(string message)
            : base(message)
        { }

        protected ItemNotAvailableException(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        { }

        public ItemNotAvailableException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
