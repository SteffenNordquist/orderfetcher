﻿using DN_Classes.Products;
using PriceCalculation.Shipping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhereToBuyDLL
{
    public class BuyPriceRow
    {
        public string name;
        public int stock;
        public double price;
        public double weightFee;
        public string sku;
        public string fixedPrice;
        public double tax;
        public string suppliername;
        public int packingUnit;
        public string artnr;
        public SimpleSize simpleSize; 


        public BuyPriceRow(string name, int stock, double price, double weightFee, string sku, int packinUnit)
        {
            this.name = name;
            this.stock = stock;
            this.price = price;
            this.weightFee = weightFee;
            this.sku = sku;
        }

        //not the original
        public BuyPriceRow(string name, int stock, double price, double weightFee, string sku, string fixedPrice, double tax, string suppliername, int packinUnit, int length , int width , int height, int weight, string artnr)
        {
            this.name = name;
            this.stock = stock;
            this.price = price;
            this.weightFee = weightFee;
            this.sku = sku;
            this.fixedPrice = fixedPrice;
            this.tax = tax;
            this.suppliername = suppliername;
            this.simpleSize = new SimpleSize(length, width, height, weight);
            this.artnr = artnr;
        }

        public BuyPriceRow(string sku)
        {
            this.name = "not available";
            this.stock = 0;
            this.price = 0;
            this.weightFee = 0;
            this.sku = sku;
            this.fixedPrice = "02";
            this.tax = 0;
            this.suppliername = "not available";
            this.simpleSize = new SimpleSize(0, 0, 0, 0);
            this.artnr = "not available";
        }

        //public override string ToString()
        //{
        //    return fillBlanks(name, 10) + "\t" + fillBlanks(stock.ToString(), 10) + "\t" + fillBlanks(price.ToString(), 10) + "\t" + fillBlanks(weightFee.ToString(), 10) + "\t" + sku ; 
        //}

        public override string ToString()
        {//
            //  return this.name + "\t" + this.stock.ToString() + "\t" + this.price.ToString() + "\t" + weightFee.ToString() + "\t" + sku + "\t" + this.tax.ToString() + "\t" + suppliername + "\t" + this.fixedPrice;
            return fillBlanks(name, 10) + "\t" + fillBlanks(stock.ToString(), 10) + "\t" + fillBlanks(price.ToString(), 10) + "\t" + fillBlanks(weightFee.ToString(), 10) + "\t" + sku;
        }

        private string fillBlanks(string name, int blanks)
        {
            int chars = name.Count();

            int more = Math.Max(0, blanks - chars);

            for (int i = 0; i < more; i++)
            {
                name += " ";
            }

            return name;
        }
    }
}
