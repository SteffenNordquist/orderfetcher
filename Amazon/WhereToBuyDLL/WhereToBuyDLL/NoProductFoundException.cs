﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WhereToBuyDLL
{
    [Serializable]
    public class NoProductFoundException : Exception
    {
        public NoProductFoundException(string message)
            : base(message)
        { }

        protected NoProductFoundException(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        { }

        public NoProductFoundException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
