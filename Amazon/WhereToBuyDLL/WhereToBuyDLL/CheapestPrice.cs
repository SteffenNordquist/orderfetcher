﻿using DN_Classes;
using DN_Classes.Products;
using DN_Classes.Products.Price;
using DN_Classes.Supplier;
using PriceCalculation;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using DN_Classes.Rules.StockRules;
using DN_Classes.DBCollection.Supplier;
using MongoDB.Driver.Builders;
using System.Globalization;

namespace WhereToBuyDLL
{
	public class WhereToBuy
	{
		public string GetCheapestPriceByEan(string ean)
		{
			string resultStringBuilder = "";
			ProductFinder productFinder = new ProductFinder();

			List<string> excludedSuppliersFromOrder = ExcludedSuppliers.GetExcludedSupplierNames();
			//   excludedSuppliersFromOrder.add
			var products = productFinder.FindAllProductsByEan(ean, excludedSuppliersFromOrder);
			resultStringBuilder += System.Environment.NewLine + ean;
			List<BuyPriceRow> buyPriceRowList = new List<BuyPriceRow>();
			SupplierDB supplierDB = new SupplierDB();
			foreach (var product in products)
			{
				if (product != null)
				{
					string supplierClassName = product.GetType().ToString();
					string supplierName = cleanName(supplierClassName);
					int multiplier = (supplierName != "FABERCASTELL") ? 1 : getMultiplier(product.GetSKUList(), product.getSKU().Split('-')[1]);
					var contractConditions = supplierDB.GetSpecificContractCondition(supplierName);
					double buyPrice = (PriceCalc.getBuyPriceFromDiscount(
																		product.getPrice(),
																		product.getTax(),
																		product.getDiscount())
												* multiplier);
					double weightFee = PriceCalc.getWeightFee((int)product.getWeight(), contractConditions.WeightFee);
					double discountedBuyPrice = buyPrice - buyPrice * contractConditions.Discount / 100;
					discountedBuyPrice = discountedBuyPrice - discountedBuyPrice * contractConditions.Bonus / 100;
					double finalPrice = discountedBuyPrice + weightFee;
					string FixedPrice = (product.getPriceType() == PriceType.fixedPrice) ? "04" : "02";
					buyPriceRowList.Add(new BuyPriceRow(ean, product.getStock(), finalPrice, weightFee, product.getSKU(), FixedPrice, product.getTax(), supplierName, product.GetPackingUnit(), product.getLength(), product.getWidth(), product.getHeight(), (int)product.getWeight(), product.getArticleNumber()));
				}
			}
			//if (buyPriceRowList.Count == 0)
			//{
			//    throw new NoProductFoundException(string.Format("Product {0} was not found", ean));
			//}
			var listInStock = buyPriceRowList.Where(x => x.stock > 0).OrderBy(x => x.price).ToList();
			var listifZeroInStock = buyPriceRowList.OrderBy(x => x.price).ToList();

			if (listInStock.Count > 0)
			{
				return listInStock[0].name + "\t" +
					listInStock[0].stock + "\t" +
					listInStock[0].price + "\t" +
					listInStock[0].weightFee + "\t" +
					listInStock[0].sku + "\t" +
					listInStock[0].tax + "\t" +
					listInStock[0].suppliername + "\t" +
					listInStock[0].fixedPrice;
			}
			else
			{
				try
				{
					return listifZeroInStock[0].name + "\t" +
								 listifZeroInStock[0].stock + "\t" +
								 listifZeroInStock[0].price + "\t" +
								 listifZeroInStock[0].weightFee + "\t" +
								 listifZeroInStock[0].sku + "\t" +
								 listifZeroInStock[0].tax + "\t" +
								 listifZeroInStock[0].suppliername + "\t" +
								 listifZeroInStock[0].fixedPrice;
				}
				catch { return ""; }
			}

		}

		public string GetCheapestPriceByEanForJTL(string ean)
		{
			string resultStringBuilder = "";
			ProductFinder productFinder = new ProductFinder();
			List<string> excludedSuppliersFromOrder = ExcludedSuppliers.GetExcludedSupplierNames();
			excludedSuppliersFromOrder.Add("JTL");
			var products = productFinder.FindAllProductsByEan(ean, excludedSuppliersFromOrder);
			resultStringBuilder += System.Environment.NewLine + ean;
			List<BuyPriceRow> buyPriceRowList = new List<BuyPriceRow>();
			SupplierDB supplierDB = new SupplierDB();
			foreach (var product in products)
			{
				if (product != null)
				{
					string supplierClassName = product.GetType().ToString();
					string supplierName = cleanName(supplierClassName);
					int multiplier = (supplierName != "FABERCASTELL") ? 1 : getMultiplier(product.GetSKUList(), product.getSKU().Split('-')[1]);
					var contractConditions = supplierDB.GetSpecificContractCondition(supplierName);
					double buyPrice = (PriceCalc.getBuyPriceFromDiscount(
																		product.getPrice(),
																		product.getTax(),
																		product.getDiscount())
												* multiplier);

					double weightFee = PriceCalc.getWeightFee((int)product.getWeight(), contractConditions.WeightFee);
					double discountedBuyPrice = buyPrice - buyPrice * contractConditions.Discount / 100;
					discountedBuyPrice = discountedBuyPrice - discountedBuyPrice * contractConditions.Bonus / 100;
					double finalPrice = discountedBuyPrice + weightFee;
					string FixedPrice = (product.getPriceType() == PriceType.fixedPrice) ? "04" : "02";
					int stock = product.getStock();
					buyPriceRowList.Add(new BuyPriceRow(ean, product.getStock(), finalPrice, weightFee, product.getSKU(), FixedPrice, product.getTax(), supplierName, product.GetPackingUnit(), product.getLength(), product.getWidth(), product.getHeight(), (int)product.getWeight(), product.getArticleNumber()));
				}
			}

			var listInStock = buyPriceRowList.Where(x => x.stock > 0).OrderBy(x => x.price).ToList();
			var listifZeroInStock = buyPriceRowList.OrderBy(x => x.price).ToList();

			if (listInStock.Count > 0)
			{
				return listInStock[0].name + "\t" +
					listInStock[0].stock + "\t" +
					listInStock[0].price + "\t" +
					listInStock[0].weightFee + "\t" +
					listInStock[0].sku + "\t" +
					listInStock[0].tax + "\t" +
					listInStock[0].suppliername + "\t" +
					listInStock[0].fixedPrice;
			}
			else
			{
				try
				{
					return listifZeroInStock[0].name + "\t" +
								 listifZeroInStock[0].stock + "\t" +
								 listifZeroInStock[0].price + "\t" +
								 listifZeroInStock[0].weightFee + "\t" +
								 listifZeroInStock[0].sku + "\t" +
								 listifZeroInStock[0].tax + "\t" +
								 listifZeroInStock[0].suppliername + "\t" +
								 listifZeroInStock[0].fixedPrice;
				}
				catch { return ""; }
			}

		}

		public BuyPriceRow GetCheapestPriceByEan(string ean, string asin)
		{
			string resultStringBuilder = "";

			ProductFinder productFinder = new ProductFinder();
			List<string> excludedSuppliersFromOrder = ExcludedSuppliers.GetExcludedSupplierNames();
			var products = productFinder.FindAllProductsByEan(ean, excludedSuppliersFromOrder);

			resultStringBuilder += System.Environment.NewLine + ean;

			List<BuyPriceRow> buyPriceRowList = new List<BuyPriceRow>();
			SupplierDB supplierDB = new SupplierDB();

			foreach (var product in products)
			{
				try
				{
					if (product != null)
					{
						string supplierClassName = product.GetType().ToString();
						string supplierName = cleanName(supplierClassName);

						int multiplier = (!new TextCleaner().getAllSuppliersWithMultipliers().Contains(supplierName)) ? 1 : getMultiplier(product.GetSKUList(), asin);

						string properSku = getProperSKU(product.GetSKUList(),asin);
						var contractConditions = supplierDB.GetSpecificContractCondition(supplierName);

						double buyPrice = (PriceCalc.getBuyPriceFromDiscount(
																			product.getPrice(),
																			product.getTax(),
																			product.getDiscount())
													* multiplier);

						double weightFee = PriceCalc.getWeightFee((int)product.getWeight(), contractConditions.WeightFee);

						double discountedBuyPrice = buyPrice - buyPrice * contractConditions.Discount / 100;
						discountedBuyPrice = discountedBuyPrice - discountedBuyPrice * contractConditions.Bonus / 100;


						//( "JTL" == supplierName.ToUpper()) ? weightFee :
						double finalPrice = ("JTL" == supplierName.ToUpper()) ? buyPrice : discountedBuyPrice + weightFee;


						string FixedPrice = (product.getPriceType() == PriceType.fixedPrice) ? "04" : "02";
						buyPriceRowList.Add(new BuyPriceRow(ean, product.getStock(), finalPrice, weightFee, properSku, FixedPrice, product.getTax(), supplierName, product.GetPackingUnit(), product.getLength(), product.getWidth(), product.getHeight(), (int)product.getWeight(), product.getArticleNumber()));

					}
				}
				catch
				{

				}
			}

			if (buyPriceRowList.Count == 0)
			{
				throw new NoProductFoundException(string.Format("Product {0} was not found", ean));
			}

			//var listInStock = buyPriceRowList.Where(x => x.stock > 0).OrderBy(x => x.price).ToList();

			var listInStock = buyPriceRowList.Where(x => x.stock > 0 && x.suppliername != "JTL").OrderBy(x => x.price).ToList();
			var JTLBuyPriceRow = buyPriceRowList.Where(x=> x.suppliername == "JTL").ToList();

			SoldAndUnshipStockDeductionRule rule = new SoldAndUnshipStockDeductionRule();

			int finalStock = 0;

			if (JTLBuyPriceRow.Count > 0)
			{
				finalStock = rule.GetFinalStock(JTLBuyPriceRow[0].name, JTLBuyPriceRow[0].stock);
			}

			if (finalStock > 0)
			{
				return JTLBuyPriceRow[0];
			}
			else
			{
				if (listInStock.Count > 0)
				{
					return listInStock[0];
				}
				else
				{
					throw new ItemNotAvailableException("Item " + ean + " is not available");
				}
			}


			//if (listInStock.Count > 0)
			//{
			//	if (listInStock.Count > 1)
			//	{
			//		if (listInStock[0].suppliername == "JTL")
			//		{
			//			int finalStock = rule.GetFinalStock(listInStock[0].name, listInStock[0].stock);

			//			if (finalStock > 0)
			//			{
			//				return listInStock[0];
			//			}
			//			else
			//			{
			//				return listInStock[1];
			//			}
			//		}

			//	}

			//	return listInStock[0];
			//}
			//else
			//{
			//	throw new ItemNotAvailableException("Item " + ean + " is not available");


			//}

		}

		public static string cleanName(string rawName)
		{
			rawName = rawName.Split('.').Last().Replace("ENTITY", "").Replace("entity","").Replace("Entity","");

			if (rawName == null)
			{
				throw new ArgumentNullException("'Supplier Name' must be set");
			}

			//flotten supplier special due to bad naming convention
			//missing '24'
			if (rawName.ToLower().Contains("flotten"))
			{
				rawName = "FLOTTEN24";
			}

			//3m supplier special due to bad naming convention
			//missing '_' in supplier collection
			if (rawName.ToLower().Contains("3m"))
			{
				rawName = "3m";
			}

			SupplierDBCollection suppCollection = new SupplierDBCollection();
			var supplierList = suppCollection.SupplierCollection.FindAll().SetFields("_id").Select(x=> x._id.ToString()).ToList();

			foreach (var eachSupplier in supplierList)
			{
				if (eachSupplier.ToUpper().Contains(rawName.ToUpper()))
				{
					return eachSupplier;
				}
			}
			//var supplier = suppCollection.SupplierCollection.FindOne( Query.EQ( "_id", rawName.ToUpper().Trim() ) );

			//if (supplier == null)
			//{
			//	supplier = suppCollection.SupplierCollection.FindOne(Query.EQ("_id", CultureInfo.CurrentCulture.TextInfo.ToTitleCase(rawName.Trim())));

			//}

			return "Supplier Name Not Found";

		}

		private static int getMultiplier(List<string> skuList, string asin)
		{
			int multiplier = 1;

			try
			{
				string getSKU = skuList.Where(x => x.Contains(asin)).First();
				multiplier = Convert.ToInt32(getSKU.Split('-')[3]);
			}

			catch { }

			return multiplier;
		}

		private static string getProperSKU(List<string> skuList, string asin)
		{
			string sku = skuList.First();

			try
			{
				sku = skuList.Where(x => x.Contains(asin)).First();
				//multiplier = Convert.ToInt32(getSKU.Split('-')[3]);
			}

			catch { }

			return sku;
		}
	}
}