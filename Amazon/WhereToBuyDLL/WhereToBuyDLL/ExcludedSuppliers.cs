﻿using DN_Classes.Supplier;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using MongoDB.Bson;
namespace WhereToBuyDLL
{
    public class ExcludedSuppliers
    {
        private static SupplierDB supplierDB = new SupplierDB();

        public static List<string> GetExcludedSupplierNames()
        {
         
            List<string> excludedSupplier = new List<string>();
            supplierDB.Suppliers
                .ToList()
                .Where(x => x.plus != null && IsExclude(x.plus.ToJson()))
                .ToList()
                .ForEach(i => excludedSupplier.Add(i._id));
            return excludedSupplier;
        }

        public static List<string> GetExcludedSupplierNamesForJTL()
        {

            List<string> excludedSupplier = new List<string>();
            supplierDB.Suppliers
                .ToList()
                .Where(x => x.plus != null && IsExclude(x.plus.ToJson()))
                .ToList()
                .ForEach(i => excludedSupplier.Add(i._id));
            excludedSupplier.Add("JTL");
            return excludedSupplier;
        }

        private static bool IsExclude(string plus)
        {
            dynamic plusObject = JObject.Parse(plus);

            if (plusObject.ExcludeFromSupplierOrder != null && plusObject.ExcludeFromSupplierOrder == true)
            {
                return true;
            }

            return false;
        }
    }
}
