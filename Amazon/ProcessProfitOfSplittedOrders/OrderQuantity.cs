﻿
using DN_Classes.Amazon.Orders;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessProfitOfSplittedOrders
{
    public class OrderQuantity
    {
		/// <summary>
		/// Split items by Number of quantity
		/// </summary>
		/// <param name="totalOrdersTemp"></param>
		/// <returns></returns>
        public List<AmazonOrder> SplitQuantity(List<AmazonOrder> totalOrdersTemp)
        {
            List<AmazonOrder> totalOrders = new List<AmazonOrder>();
            foreach (var order in totalOrdersTemp)
            {
                try
                {
                    int quantity = int.Parse(order.QuantityOrdered.AsString);
                    double itemPrice = 0;
                    try
                    {
                        itemPrice = double.Parse(order.ItemPrice["Amount"].ToString(), CultureInfo.InvariantCulture);
                    }
                    catch {
                        var convertPrice = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(order.ItemPrice.AsString); 

                    
                    }
                    var newItemPrice = double.Parse(order.ItemPrice["Amount"].ToString(), CultureInfo.InvariantCulture) / quantity;

                    for (int i = 0; i < quantity; i++)
                    {
                        var orderClone = (AmazonOrder)order.Clone();
                        orderClone.QuantityOrdered = 1;
                        orderClone.ItemPrice = newItemPrice;

                        totalOrders.Add(orderClone);
                    }
                }
                catch { }
            }

            return totalOrders;
        }
    }
}
