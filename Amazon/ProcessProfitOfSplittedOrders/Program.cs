﻿using DN_Classes.Amazon.Orders;
using DN_Classes.AppStatus;
using DN_Classes.Entities;
using DN_Classes.Supplier;
using InsertOrderXmlToMongoDb;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProcessProfitOfOrderDll;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProcessProfitOfSplittedOrders
{
	public class Program
	{

		//static ProcessProfitOfOrderDll.SingleOrderDB singleOrdersDB = new ProcessProfitOfOrderDll.SingleOrderDB();
		//static CalculateOrders processOrders = new CalculateOrders();
		//static SingleOrderItems getSingleOrderItems = new SingleOrderItems();
		//static InsertAndUpdateToDatabase toDatabase = new InsertAndUpdateToDatabase();

		public static void Main()
		{
			Console.WriteLine("Old tool is running.");
			do
			{
				runMain();
				// When task scheduler is weird. 
				if (Properties.Settings.Default.Loop)
				{
					Console.WriteLine("Sleeping for 2 minutes...");
					Thread.Sleep(TimeSpan.FromMinutes(2));
				}
			} while (Properties.Settings.Default.Loop);

		}


		public static void runMain()
		{
			List<string> logFile = File.ReadAllLines(Properties.Settings.Default.LogPath).ToList();
			ProcessProfitOfOrderDll.SingleOrderDB singleOrdersDB = new ProcessProfitOfOrderDll.SingleOrderDB();
			CalculateOrders processOrders = new CalculateOrders();
			SingleOrderItems getSingleOrderItems = new SingleOrderItems();
			InsertAndUpdateToDatabase toDatabase = new InsertAndUpdateToDatabase();

			/// Update bugs Supplier.
			var getAllFiles = File.ReadAllLines(@"C:\DevNetworks\bugs.txt").ToList().Select(x => x.Split('\t')[4]).ToList();
			var getOrdersample = singleOrdersDB.sampleOrders(getAllFiles);
			updateBugItems(getOrdersample);
			Console.WriteLine("Close the tool");
			var asdfasdf = Console.ReadLine();
			Thread.Sleep(TimeSpan.FromHours(100));

			
			var getOrders = singleOrdersDB.GetOrders();
			ItemQualityChecking();

			// Amazon Error - Item that has 1 asin/item but plenty of prices or plenty of asins but one price
			var getAmazonError = getOrders.Where(x => x.Order.ASIN.IsBsonArray && !x.Order.ItemPrice.IsBsonArray).ToList();

			// Remove Amazon Error items in real items to be calculated.
			var getNewOrdersAmazonOrderIdList = getAmazonError.Select(x => x.Order.AmazonOrderId).ToList();
			getOrders = getOrders.Where(x => !getNewOrdersAmazonOrderIdList.Contains(x.Order.AmazonOrderId)).ToList();

			// Split item by number of Items (asins) and number of stocks (quantity)
			List<AmazonOrder> totalOrders = getSingleOrderItems.SplitOrderItems(getOrders);
			Console.WriteLine("Number of orders " + totalOrders.Count());

			/// Calculate all amazon splitted orders
			var allCalculatedOrders = processOrders.ProcessAllItems(totalOrders);

			Console.WriteLine("New orders:" + allCalculatedOrders.Count());

			foreach (var singleorder in allCalculatedOrders)
			{
				Console.WriteLine(singleorder.Key.AmazonOrderId + "\t imported.");
				toDatabase.SendToDatabase(singleorder.Key,
										  singleorder.Value);

			}

			//var ids = toDatabase.getAllAvailableIDsOrdersForAmazon();
			var getUnavailableOrders = singleOrdersDB.GetUnAvailableItemsForAmazon(); //sampleOrders();// 

			// Same process in calculating available.
			List<AmazonOrder> totalUnavailableOrders = getUnavailableOrders.Select(x => x.Order).ToList();//getSingleOrderItems.SplitOrderItems(getUnavailableOrders).ToList();//.Where(x => filterIds(x, ids)).ToList();

			Console.WriteLine("Bug orders:" + totalUnavailableOrders.Count());
			var allUnavailableCalculatedOrders = processOrders.ProcessAllItems(totalUnavailableOrders);
			int countFoundOrders = 0;
			foreach (var order in allUnavailableCalculatedOrders)
			{
				if (order.Value.Item.Profit != -3.33 && order.Value.Shipping.ShippingName != "not available")
				{
					countFoundOrders++;
					Console.WriteLine(order.Key.AmazonOrderId + "\t updated to available.");
					toDatabase.updateFoundOrder(order.Key, order.Value);
				}
			}

			Console.WriteLine("Amazon error orders:" + getAmazonError.Count());

			var totalAmazonError = getSingleOrderItems.SplitOrderItems(getAmazonError);
			// Just import directly to our database because its a weird item.
			foreach (var item in totalAmazonError)
			{
				var error = new Default_Error().setDefaulterros(item);

				Console.WriteLine(item.AmazonOrderId + "\t Amazon Error imported.");
				toDatabase.SendToDatabase(item, error);
			}


			#region Log
			logFile.Add("************************************************");
			logFile.Add("Date:" + DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"));
			//logFile.Add("Number of available orders: " + getOrders.Count() + " and Number of Splitted:" + totalOrders.Count() + "\n\n");
			logFile.Add("Number of not available orders: " + totalUnavailableOrders.Count());
			logFile.Add("Number of unavailable to available orders: " + countFoundOrders + "\n\n");
			logFile.Add("Number of amazon error orders: " + getAmazonError.Count() + "\n\n");
			logFile.Add("***********************************************");
			File.WriteAllLines(Properties.Settings.Default.LogPath, logFile.ConvertAll(Convert.ToString));
			#endregion

			Thread.Sleep(TimeSpan.FromSeconds(10));
		}

		/// <summary>
		/// Quality checking and updates items based on probability percentage.
		/// </summary>
		private static void ItemQualityChecking()
		{
			CalculatedProfitDB cpdb = new CalculatedProfitDB();
			Random random = new Random();
			using (ApplicationStatus appStatus = new ApplicationStatus("ProcessProfit_Amazon_ItemQuality_148"))
			{
				try
				{
					var before30DaysItems = cpdb.collection.Find(Query.And(Query.GTE("PurchasedDate", DateTime.Now.AddDays(-30)) ,
																			Query.Or(Query.And( 
																						Query.Exists("plus.QualityChecked"),
																						Query.EQ("plus.QualityChecked",false) )
																			,Query.NotExists("plus.QualityChecked"))
																			))
												.SetFields("PurchasedDate", "AmazonOrder.AmazonOrderId", "SupplierName","plus")
						.Where(x => x.PurchasedDate.Date > DateTime.Now.AddDays(-31).Date ).ToList();

					var todaysItems = before30DaysItems.Where(x => x.PurchasedDate.Date > DateTime.Now.AddDays(-1).Date).ToList();
					var probability = 0.01;
					int count = 0;

					#region Probability 1%
					Console.WriteLine("**********************************************");
					Console.WriteLine("Updated Items for todays items.\n\n");
					
					foreach (var item in todaysItems)
					{

						var nextDouble = random.NextDouble();
						if (nextDouble <= probability)
						{
							count++;
							Console.WriteLine(count + ":" + item.AmazonOrder.AmazonOrderId + "\t" + item.PurchasedDate.ToString("MM.dd.yyyy"));
							cpdb.collection.Update(Query.EQ("AmazonOrder.AmazonOrderId", item.AmazonOrder.AmazonOrderId),
										  Update.Set("plus.QualityChecked", false), UpdateFlags.Multi);
						}
					}

					Console.WriteLine("\n\n**********************************************");
					#endregion

					var groupedOrders = before30DaysItems.GroupBy(x => x.SupplierName).ToList();
					foreach (var supplier in groupedOrders)
					{

						#region Probability 100%
						probability = 1.00;
						var supplierItems = supplier.ToList();

						if (supplierItems.Count() <= 30)
						{
							Console.WriteLine("**********************************************");
							Console.WriteLine("Updated Item for " + supplier.Key + " supplier for below 31 items.\n\n");
							
							count = 0;
							foreach (var item in supplierItems)
							{
								var nextDouble = random.NextDouble();
								if (nextDouble <= probability)
								{
									count++;
									Console.WriteLine(count + ":" + item.AmazonOrder.AmazonOrderId + "\t" + item.PurchasedDate.ToString("MM.dd.yyyy"));
									cpdb.collection.Update(Query.EQ("AmazonOrder.AmazonOrderId", item.AmazonOrder.AmazonOrderId),
												  Update.Set("plus.QualityChecked", false), UpdateFlags.Multi);
								}
							}

							Console.WriteLine("\n\n**********************************************");
						}
						#endregion

						#region Probability 25%
						probability = 0.25;
						if (supplierItems.Count() <= 100)
						{
							Console.WriteLine("**********************************************");
							Console.WriteLine("Updated Item for " + supplier.Key + " supplier for below 101 items.\n\n");
							
							count = 0;
							foreach (var item in supplierItems)
							{
								var nextDouble = random.NextDouble();

								if (nextDouble <= probability)
								{
									count++;
									Console.WriteLine(count + ":" + item.AmazonOrder.AmazonOrderId + "\t" + item.PurchasedDate.ToString("MM.dd.yyyy"));
									cpdb.collection.Update(Query.EQ("AmazonOrder.AmazonOrderId", item.AmazonOrder.AmazonOrderId),
												  Update.Set("plus.QualityChecked", false), UpdateFlags.Multi);
								}
							}

							Console.WriteLine("\n\n**********************************************");
						}
						#endregion

					}

					Console.WriteLine("Sleeping for 30 seconds to start calculating");
					for (int i = 1; i <= 30; i++)
					{
						Console.Write("*");
						Thread.Sleep(1000);
					}

					appStatus.AddMessagLine("Success!");
					appStatus.Successful();
				}
				catch(Exception ex)
				{
					appStatus.AddMessagLine(ex.InnerException.Message.ToString());
				}
			}
		}

		private static void updateBugItems(List<ProcessProfitOfOrderDll.SingleOrderItem> getOrders)
		{
			SingleOrderItems getSingleOrderItems = new SingleOrderItems();
			CalculateOrders processOrders = new CalculateOrders();
			InsertAndUpdateToDatabase toDatabase = new InsertAndUpdateToDatabase();

			List<AmazonOrder> totalUnavailableOrders = getSingleOrderItems.SplitOrderItems(getOrders).ToList();//.Where(x => filterIds(x, ids)).ToList();

			Console.WriteLine("Unavailable orders:" + totalUnavailableOrders.Count());
			//var temp = new List<AmazonOrder>();
			//temp.Add(totalUnavailableOrders.First());
			//totalUnavailableOrders = temp;

			var allUnavailableCalculatedOrders = processOrders.ProcessAllItems(totalUnavailableOrders);
			int countFoundOrders = 0;
			foreach (var order in allUnavailableCalculatedOrders)
			{
				if (true)//(order.Value.Item.Profit != -3.33 && order.Value.Shipping.ShippingName != "not available") || Properties.Settings.Default.IncludeNotAvailable)
				{
					countFoundOrders++;
					Console.WriteLine(order.Key.AmazonOrderId + "\t updated to available.");
					toDatabase.updateFoundBugOrder(order.Key, order.Value);
				}
			}
		}

		private static bool filterIds(AmazonOrder x, Dictionary<string, List<string>> ids)
		{
			foreach (var item in ids)
			{
				if (item.Key == x.AmazonOrderId)
				{
					return item.Value.ToList().Contains(x.SellerSKU.AsString.ToString());
				}
			}

			return false;
		}

		public static void updateOrders(List<AmazonOrder> allOrders)
		{
			ProcessProfitOfOrderDll.SingleOrderDB singleOrdersDB = new ProcessProfitOfOrderDll.SingleOrderDB();


			foreach (var order in allOrders)
			{
				singleOrdersDB.collection.Update(Query.EQ("Order.AmazonOrderId", order.AmazonOrderId),
								Update.Unset("plus.OrderProcessed"));

				Console.WriteLine(order.AmazonOrderId.ToString() + "\t updated");
			}

		}

	}
}
