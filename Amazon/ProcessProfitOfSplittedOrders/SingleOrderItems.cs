﻿
using DN_Classes.Amazon.Orders;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProcessProfitOfOrderDll;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessProfitOfSplittedOrders
{
    public class SingleOrderItems
    {
        //static SingleOrderDB singleOrdersDB = new SingleOrderDB();

		/// <summary>
		/// Splits item by asin and/or quantity
		/// </summary>
		/// <param name="orders"></param>
		/// <returns>List<AmazonOrder></returns>
        public List<AmazonOrder> SplitOrderItems(List<SingleOrderItem> orders)
        {
            //var orders = singleOrdersDB.GetOrders();

            var singleOrder = orders.Where(x => x.Order.ASIN.IsString).ToList();
            var multiorders = orders.Where(x => x.Order.ASIN.IsBsonArray).ToList();

            var totalOrdersTemp = singleOrder.Select(x => x.Order).ToList();

            MultiOrders splitMultiOrders = new MultiOrders();
            totalOrdersTemp.AddRange(splitMultiOrders.SplitMultiOrders(multiorders));

            OrderQuantity splitQuantity = new OrderQuantity();

            List<AmazonOrder> totalOrders = splitQuantity.SplitQuantity(totalOrdersTemp);
            return totalOrders;
        }
    }
}
