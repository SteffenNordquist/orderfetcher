﻿
using DN_Classes.Amazon.Orders;
using MongoDB.Bson;
using ProcessProfitOfOrderDll;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessProfitOfSplittedOrders
{
    public class MultiOrders
    {
		/// <summary>
		/// Splits items by asins
		/// </summary>
		/// <param name="multiorders"></param>
		/// <returns></returns>
        public List<AmazonOrder> SplitMultiOrders(List<SingleOrderItem> multiorders)
        {
            List<AmazonOrder> totalOrdersTemp = new List<AmazonOrder>();
            foreach (var multiOrder in multiorders)
            {
                int count = multiOrder.Order.ASIN.AsBsonArray.Count();

                AmazonOrder amazonOrder = multiOrder.Order;
                for (int i = 0; i < count; i++)
                {
                    try
                    {
                        AmazonOrder amazonOrderTemplate = (AmazonOrder)multiOrder.Order.Clone();

                        #region

                        amazonOrderTemplate.ASIN = multiOrder.Order.ASIN.AsBsonArray[i];
                        amazonOrderTemplate.SellerSKU = multiOrder.Order.SellerSKU.AsBsonArray[i];
                        amazonOrderTemplate.OrderItemId = multiOrder.Order.OrderItemId.AsBsonArray[i];
                        amazonOrderTemplate.Title = multiOrder.Order.Title.AsBsonArray[i];
                        amazonOrderTemplate.QuantityOrdered = multiOrder.Order.QuantityOrdered.AsBsonArray[i];
                        amazonOrderTemplate.QuantityShipped = multiOrder.Order.QuantityShipped.AsBsonArray[i];
						try
						{
							if (multiOrder.Order.ItemPrice.AsBsonValue[i].IsBsonDocument)
							{
								amazonOrderTemplate.ItemPrice = multiOrder.Order.ItemPrice.AsBsonValue[i];
							}
							else
							{
								amazonOrderTemplate.ItemPrice = multiOrder.Order.ItemPrice.AsBsonDocument;
							}
						}
						catch
						{
							amazonOrderTemplate.ItemPrice = multiOrder.Order.ItemPrice.AsBsonValue[0];
						}
                        amazonOrderTemplate.ShippingPrice = multiOrder.Order.ShippingPrice.AsBsonValue[i];
                        amazonOrderTemplate.GiftWrapPrice = multiOrder.Order.GiftWrapPrice.AsBsonValue[i];
                        amazonOrderTemplate.ItemTax = multiOrder.Order.ItemTax.AsBsonValue[i];
                        amazonOrderTemplate.ShippingTax = multiOrder.Order.ShippingTax.AsBsonValue[i];
                        amazonOrderTemplate.GiftWrapTax = multiOrder.Order.GiftWrapTax.AsBsonValue[i];
                        amazonOrderTemplate.ShippingDiscount = multiOrder.Order.ShippingDiscount.AsBsonValue[i];
                        amazonOrderTemplate.PromotionDiscount = multiOrder.Order.PromotionDiscount.AsBsonValue[i];
						try
						{
							amazonOrderTemplate.ConditionNote = multiOrder.Order.ConditionNote.AsBsonValue[i];
						}
						catch
						{

						}
                        
                        amazonOrderTemplate.ConditionId = multiOrder.Order.ConditionId.AsBsonValue[i];
                        amazonOrderTemplate.ConditionSubtypeId = multiOrder.Order.ConditionSubtypeId.AsBsonValue[i];

                        #endregion

                        totalOrdersTemp.Add(amazonOrderTemplate);
                    }
                    catch { }
                }
            }
            return totalOrdersTemp;
        }
    }
}
