﻿using DN_Classes.Amazon.Orders;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProcessProfitOfOrderDll;
using ProcessProfitOfSplittedOrders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderTesting
{
    class Program
    {
        static SingleOrderDB singleOrdersDB = new SingleOrderDB();
        static CalculateOrders processOrders = new CalculateOrders();
        static SingleOrderItems getSingleOrderItems = new SingleOrderItems();

        static void Main(string[] args)
        {

            //List<AmazonOrder> totalOrders = getSingleOrderItems.GetSingleOrderItems(getQuery());
            //var allCalculatedOrders = processOrders.ProcessAllItems(totalOrders);
            //var sample = Console.ReadLine();
        }


        public static IMongoQuery getQuery()
        {
            return Query.And(Query.NE("Order.OrderStatus", "Pending"),
                                           Query.NE("Order.OrderStatus", "Canceled"),
                                           Query.NotExists("plus.OrderProcessed"));
        }
    }
}
