﻿
using DN_Classes.Amazon.Orders;
//using InvoiceGenerator;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsertOrderXmlToMongoDb
{
    public class SingleOrderItem
    {
       
        public ObjectId id { get; set; }
        public AmazonOrder Order { get; set; }
        public BsonDocument plus { get; set; }

        public SingleOrderItem()
        {
           
        }
      
    }
}
