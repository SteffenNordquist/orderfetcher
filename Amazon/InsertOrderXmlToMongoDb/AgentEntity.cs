﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
namespace InsertOrderXmlToMongoDb
{
    public class AgentEntity
    {
        public ObjectId Id { get; set; }
        public BsonDocument agent { get; set; }

        internal string GetFinancialAuthority()
        {
            try
            {
                return agent["financialauthorities"].AsString;
            }
            catch
            {
                return "";
            }
        }
    }
}
