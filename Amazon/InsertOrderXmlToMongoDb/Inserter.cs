﻿using MongoDB.Bson;
using MongoDB.Driver;

using Newtonsoft.Json;
using System;
using System.Xml;

//using InvoiceGenerator;
using MongoDB.Driver.Builders;

namespace InsertOrderXmlToMongoDb
{
    public class Inserter
    {

        public static MongoCollection<AgentEntity> AgentCollection;

	    private readonly SingleOrderDB _sodb = new SingleOrderDB(); 


        public string OrderXml;
	    private string _agentId = "";



        public Inserter(string orderXml)
        {
            OrderXml = orderXml;
        }

        public void Insert(string agentIDfromAmazon)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(OrderXml);

            foreach (XmlNode node in doc.DocumentElement.ChildNodes)
            {
                XmlElement agentIdElement = doc.CreateElement("AgentId");
                agentIdElement.InnerText = agentIDfromAmazon;
                node.AppendChild(agentIdElement);

                string jsonText = JsonConvert.SerializeXmlNode(node);
                var order = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(jsonText);

                string orderItemId = node.SelectSingleNode("./OrderItemId").InnerText;
				string amazonOrderId = node.SelectSingleNode("./AmazonOrderId").InnerText;
				string date = node.SelectSingleNode("./PurchaseDate").InnerText;

				

                var update = Update.Set("Order", order["Order"]);
                                   //.Set("plus.OrderProcessed", DateTime.Now);

                var query = Query.EQ("Order.OrderItemId", orderItemId);

				string find = _sodb.collection.FindOne(query) == null ? "Inserted: " + amazonOrderId + "\t" + date : "Already in the database: " + amazonOrderId + "\t" + date;

				Console.WriteLine(find);
				

				_sodb.collection.Update(query, update, UpdateFlags.Upsert);
            }
        }


        #region Insert to invoice collection

        //public void InsertToInvoiceData(BsonDocument orderdetail)
        //{
        //    AmazonOrder order = new AmazonOrder { orderdetail = orderdetail };

        //    string agentName = orderdetail["agent"].AsString;
        //    string amazonOrderId = order.GetPlatformOrderId();

        //    DateTime purchasedate = order.GetPurchaseDate();
        //    Address customer = order.GetCustomer();
        //    Address billing = order.GetBillingAddress();

        //    List<OrderItem> orderItems = new List<OrderItem>();
        //    //try{
        //    orderItems = order.GetOrderItems();
        //    double totalShipping = order.GetTotalShipping();
        //}    
        #endregion
    }
}
