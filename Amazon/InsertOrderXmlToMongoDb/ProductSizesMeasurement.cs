﻿
//using InvoiceGenerator;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsertOrderXmlToMongoDb
{
    public class ProductSizesMeasurement
    {
        public ObjectId id { get; set; }
        public string ean { get; set; }
        public Measure measure { get; set; }

        public class Measure
        {
            public int length { get; set; }
            public int width { get; set; }
            public int height { get; set; }
            public int weight { get; set; }
        }
    }
}
