﻿

using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsertOrderXmlToMongoDb
{
    public class ProductSizesMeasurementsDB
    {
        string ConnectionString = @"mongodb://client148:client148devnetworks@148.251.0.235/ProductSize";
        MongoClient Client = null;
        MongoServer Server = null;
        MongoDatabase Database = null;

        public MongoCollection<ProductSizesMeasurement> collection = null;

        public ProductSizesMeasurementsDB()
        {
            MongoClient Client = new MongoClient(ConnectionString);
            MongoServer Server = Client.GetServer();
            MongoDatabase Database = Server.GetDatabase("ProductSize");
            collection = Database.GetCollection<ProductSizesMeasurement>("productsizes");
        }
    }
}
