﻿

using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsertOrderXmlToMongoDb
{
    public class SingleOrderDB
    {

			string ConnectionString = @"mongodb://client148:client148devnetworks@136.243.44.111/Orders";
            MongoClient Client = null; 
            MongoServer Server = null; 
            MongoDatabase Database = null;

            public MongoCollection<SingleOrderItem> collection = null; 


        public SingleOrderDB()
        {
            MongoClient Client = new MongoClient(ConnectionString);
            MongoServer Server = Client.GetServer();
            MongoDatabase Database = Server.GetDatabase("Orders");
            collection = Database.GetCollection<SingleOrderItem>("SingleOrderItems");
        }


        public List<SingleOrderItem> GetOrders()
        {
            return collection.Find(Query.And(
                                                                Query.NE("Order.OrderStatus", "Pending"),
                                                                Query.NE("Order.OrderStatus", "Canceled"),
                                                                Query.NotExists("plus.OrderProcessed")
                                                        )).ToList();

        }

    }
}
