# README #

This README would normally document whatever steps are necessary to get your application up and running.

### GetAmazonOrders ###

* Get all orders from amazon.
* Version 1

### How do I get set up? ###

* Double check local agent ID. Pull all updated DLL including mongodb.
* Configuration [ GetAmazonOrders.exe.config ]
* Dependencies - [ DN.Classes, ProcessProfitOfOrder, mongodb ]

### Contribution guidelines ###

* Possible error:
1. WhereToBuyDLL, should always have the latest productdb dll. The methods from productdb should not have 'not implemented' exception.
2. Check database connection.
3. Check Testing methods and un-comment/comment the unnecessary methods.
4. All DLL should have the latest version.


### ProcessProfitOfSplittedOrders ###

* Calculate all orders.
* Version 1

### How do I get set up? ###

* Pull all updated DLL including mongodb. Check the testing methods and database connection. 
* Configuration [ ProcessProfitOfSplittedOrders.exe.config ]
* Dependencies - [ DN.Classes, ProcessProfitOfOrder, mongodb, ProductDB, EbayOrder, WhereToBuyDLL, PriceCalculation(profitcalc) ]

### WhereToBuyDLL ###

* Find the cheapest order from all of our databases.
* Version 1

### How do I get set up? ###

* reference this dll to applications
* Dependencies - [ productdb, dnclasses, mongodb , pricecalculation , newtonsoft]

### InsertOrderXmlToMongoDb ###

* Insert the order from amazon to our database
* Version 1

### How do I get set up? ###

* reference this dll to applications
* Dependencies - [ dnclasses, mongodb , newtonsoft]

### WhereToBuyTester ###

* Test order if it is available or not.
* Version 1

### How do I get set up? ###

* reference this dll to applications
* Dependencies - [ dnclasses, mongodb , WhereToBuyDLL]

### Who do I talk to? ###

* Rudy
* Other community or team contact