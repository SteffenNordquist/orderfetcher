﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN_Classes.Supplier;
using DN_Classes.Conditions;

namespace ProcessProfitOfOrder
{
    public class PlatformConditionByCategory
    {
        public static PlatformConditions GetPlatformConditionsFromCategory(string category, PlatformConditions platformConditionsDefault)
        {
            if (category == "book_display_on_website")
            {
                return new PlatformConditions(15, 0, 0.56);
            }
            if (category == "music_display_on_website")
            {
                return new PlatformConditions(15, 0, 0.56);
            }
            if (category == "jewelry_display_on_website")
            {
                return new PlatformConditions(20, 0, 0);
            }
            if (category == "dvd_display_on_website")
            {
                return new PlatformConditions(15, 0, 0.56);
            }
            if (category == "toy_display_on_website")
            {
                return new PlatformConditions(15, 0, 0);
            }
            if (category == "musical_instruments_display_on_website")
            {
                return new PlatformConditions(12, 0, 0);
            }
            if (category == "pc_display_on_website")
            {
                return new PlatformConditions(12, 0, 0);
            }
            if (category == "ce_display_on_website")
            {
                return new PlatformConditions(12, 0, 0);
            }
            if (category == "office_product_display_on_website")
            {
                return new PlatformConditions(15, 0, 0);
            }
            if (category == "kitchen_display_on_website")
            {
                return new PlatformConditions(15, 0, 0);
            }

            //return default, if no fit it
            return platformConditionsDefault;

        }
    }
}
