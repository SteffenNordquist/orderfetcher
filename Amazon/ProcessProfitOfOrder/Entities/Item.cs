﻿using DN_Classes;
using DN_Classes.Conditions;
using DN_Classes.Entities;
using DN_Classes.Products;
using DN_Classes.Products.Price;
using MongoDB.Bson.Serialization.Attributes;
using PriceCalculation;
using ProcessProfitOfOrder.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WhereToBuyDLL;

namespace ProcessProfitOfOrder.Entity
{
    [BsonIgnoreExtraElements]
    public class Item: ItemEntity
    {

        public Item(BuyPriceRow buyPriceRow, ProfitConditions profitConditions, SimpleSize simpleSize, double profit )
        {
            NewBuyPrice = Math.Round(buyPriceRow.price, 2);
            DiscountedBuyprice = Math.Round(buyPriceRow.price - buyPriceRow.weightFee, 2);
            Stock = buyPriceRow.stock;
            SetRiskCosts(buyPriceRow, profitConditions);
            Profit = Math.Round(profit, 2);
            SimpleSize = simpleSize;
        }

        private void SetRiskCosts(BuyPriceRow buyPriceRow, ProfitConditions profitConditions)
        {
            if (buyPriceRow.price < 25)
            {
                RiskCost = Math.Round((buyPriceRow.price * 2) * profitConditions.RiskFactor / 100, 2);
            }
            else
            {
                RiskCost = 0;
            }
        }
    }
}
