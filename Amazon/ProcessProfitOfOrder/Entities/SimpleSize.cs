﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessProfitOfOrder.Entities
{
    [BsonIgnoreExtraElements]
    public class SimpleSize
    {
        public int length { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public int weight { get; set; }

        public bool isValid { get; set; }

        public SimpleSize(DN_Classes.Products.SimpleSize _simpleSize)
        {
            length = _simpleSize.length;
            width = _simpleSize.width;
            height = _simpleSize.height;
            weight = _simpleSize.weight;
            isValid = _simpleSize.isValid;
        }
    }
}
