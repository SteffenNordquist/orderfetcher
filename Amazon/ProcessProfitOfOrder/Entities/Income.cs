﻿using DN_Classes;
using DN_Classes.Conditions;
using DN_Classes.Entities;
using DN_Classes.Products;
using DN_Classes.Products.Price;
using MongoDB.Bson.Serialization.Attributes;
using PriceCalculation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WhereToBuyDLL;

namespace ProcessProfitOfOrder.Entity
{
    public class Income:IncomeEntity
    {
        public Income(double shippingIncome, double netShippingIncome, double sellPrice) 
        {
            ShippingIncome = Math.Round(shippingIncome,2);
            NetShippingIncome = Math.Round(netShippingIncome,2);
            SellPrice = Math.Round(sellPrice,2);          
        }


        public Income(double shippingIncome, BuyPriceRow buyPriceRow, PlatformConditions platformConditions, double sellPrice)
        {
            ShippingIncome = Math.Round(shippingIncome, 2);
            double netShippingIncome = PriceCalc.getNetShippingIncome(3, platformConditions.PaymentFeeFix, platformConditions.DefaultPlatformFeeRate / 100, buyPriceRow.tax);
            NetShippingIncome = Math.Round(netShippingIncome, 2);
            SellPrice = Math.Round(sellPrice, 2);
        }
    }
}
