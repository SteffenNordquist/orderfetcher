﻿using DN_Classes.Amazon.Orders;
using DN_Classes.Conditions;
using DN_Classes.Entities;
using DN_Classes.Supplier;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProcessProfitOfOrder.Entity
{
    public class Platform : PlatformEntity
    {
        public Platform(string category, PlatformConditions platformConditions, AmazonOrder amazonOrder)
        {
            Category = category;
            FeePercentage = Math.Round(platformConditions.DefaultPlatformFeeRate,2);
            FeeinEuro = Math.Round(FeePercentage/ 100 * (amazonOrder.GetItemPrice() + 3),2);
            VariableFixcost = Math.Round(platformConditions.PaymentFeeFix,2);
            PaymentFeeEuro = 0;
            PaymentFeePercentage = 0; 
        }
    }
}
