﻿using DN_Classes.Entities;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProcessProfitOfOrder.Entities
{
    [BsonIgnoreExtraElements]
    public class Shipping: ShippingEntity
    {

        public Shipping(string shippingName, double shippingCost, double handlingCost, double packingCost)
        {
            ShippingName = shippingName;
            ShippingCosts = Math.Round(shippingCost, 2);
            HandlingCosts = Math.Round(handlingCost, 2);
            PackingCosts = Math.Round(packingCost, 2);
        }
    }
}
