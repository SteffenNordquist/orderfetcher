﻿using InsertOrderXmlToMongoDb;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessProfitOfOrder.Entities
{
    public class ConnectionForAgents
    {
                string ConnectionString = @"mongodb://client148:client148devnetworks@148.251.0.235/Agents";
        MongoClient Client = null;
        MongoServer Server = null;
        MongoDatabase Database = null;

        public MongoCollection<AgentEntity> collection = null;

        public ConnectionForAgents()
        {
            MongoClient Client = new MongoClient(ConnectionString);
            MongoServer Server = Client.GetServer();
            MongoDatabase Database = Server.GetDatabase("Agents");
            collection = Database.GetCollection<AgentEntity>("agents");
        }
    }
}
