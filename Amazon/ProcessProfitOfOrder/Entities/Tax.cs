﻿using DN_Classes;
using DN_Classes.Amazon.Orders;
using DN_Classes.Entities;
using DN_Classes.Products;
using DN_Classes.Products.Price;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WhereToBuyDLL;

namespace ProcessProfitOfOrder.Entities
{
    [BsonIgnoreExtraElements]
    public class Tax: TaxEntity
    {

        public Tax(BuyPriceRow buyPriceRow, AmazonOrder amazonOrder)
        {
            double sellPrice = amazonOrder.GetItemPrice();

            ItemTax = sellPrice - sellPrice / ((100 + buyPriceRow.tax) / 100);
            ShippingIncomeTax = 3 - 3 / ((100 + buyPriceRow.tax) / 100);

            TaxRate = buyPriceRow.tax;
        }
    }

}
