﻿using MongoDB.Bson;
using DN_Classes.Amazon.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;
using ProcessProfitOfOrder.Entities;
using WhereToBuyDLL;
using GetAmazonOrders;
using System.Text.RegularExpressions;
using System.Globalization;
using PriceCalculation;
using DN_Classes.Supplier;
using DN_Classes.Conditions;
using DN_Classes;
using DN_Classes.Products;
using DN_Classes.Entities;
using DN_Classes.Products.Price;

namespace ProcessProfitOfOrder.Entity
{
    public class CalculatedProfit: CalculatedProfitEntity
    {

        public CalculatedProfit(string agentName, BuyPriceRow buyPriceRow, AmazonOrder amazonOrder, string category, ProductSize productSize, ConditionSet conditions, Shipping shipping, double profit)
        {
            Agentname = agentName; 
            SupplierName = buyPriceRow.suppliername;
            ArticleNumber = buyPriceRow.artnr;
            VE = buyPriceRow.packingUnit == 0 ? 1 : buyPriceRow.packingUnit;
            Ean = new Regex(@"\d{13}").Match(amazonOrder.SellerSKU.AsString).ToString();
            SetPurchaseDate(amazonOrder.PurchaseDate);
            SellerSku = amazonOrder.SellerSKU.ToString();

            this.Platform = new Platform(category, conditions.Platform, amazonOrder);
            this.Income = new Income(3, buyPriceRow, conditions.Platform, amazonOrder.GetItemPrice());
            this.Item = new Item(buyPriceRow,conditions.Profit, (SimpleSize)productSize, profit);
            this.Tax = new Tax(buyPriceRow, amazonOrder);
            this.Shipping = shipping;
            this.Packing = new Packing(productSize);
            this.AmazonOrder = amazonOrder; 
            
        }



        private void SetPurchaseDate(string purchaseDate)
        {
            if (purchaseDate.Contains("."))
            {
                PurchasedDate = DateTime.Parse(purchaseDate, new CultureInfo("de-DE"));
            }
            else
            {
                PurchasedDate = DateTime.ParseExact(purchaseDate, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
            }

            #region
            //try
            //{
            //    PurchasedDate = DateTime.ParseExact(purchaseDate, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
            //}
            //catch
            //{
            //    try
            //    {
            //        //PurchasedDate = DateTime.Parse(purchaseDate, new CultureInfo("de-DE"));
            //        PurchasedDate = DateTime.ParseExact(purchaseDate, "d/M/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
            //    }
            //    catch
            //    {
            //        PurchasedDate = DateTime.Now;
            //    }
            //}

            #endregion

        }
        
    }
}
