﻿using DN_Classes.Amazon.Orders;
using DN_Classes.Supplier;
using GetAmazonOrders;
using InsertOrderXmlToMongoDb;
using WhereToBuyDLL;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using PriceCalculation.Shipping;
using ProcessProfitOfOrder.Entity;
using ProcessProfitOfOrder.Entities;
using AgentDLL;
using PriceCalculation;
using MongoDB.Bson;
using DN_Classes;
using DN_Classes.Products;
using DN_Classes.Conditions;
using DN_Classes.AppStatus;
using DN_Classes.Entities;
using DN_Classes.Products.Price;


namespace ProcessProfitOfOrder
{
    class Program
    {
        static ProductFinder productFinder = new ProductFinder();
        static SupplierDB supplierDB = new SupplierDB();
        static CategoriesDB categoriesDB = new CategoriesDB();
        static SingleOrderDB singleOrdersDB = new SingleOrderDB();
        static ProductSizesMeasurementsDB productSizesDB = new ProductSizesMeasurementsDB();
        static CalculatedProfitDB calculatedProfitDB = new CalculatedProfitDB();
        static WhereToBuy whereToBuy = new WhereToBuy();
        static PackingList packingList = new PackingList();

        static void Main(string[] args)
        {

            using (ApplicationStatus appStatus = new ApplicationStatus("ProcessProfitOfOrder"))
            {
                List<AmazonOrder> totalOrders = GetSingleOrderItems();

                foreach (var singleItem in totalOrders)
                {
                    string category = GetCategoryFromAsin(singleItem);
                    ConditionSet conditionSet = GetConditionSet(category);

                    string ean = new Regex(@"\d{13}").Match(singleItem.SellerSKU.AsString).ToString();
                    string agentName = GetAgentName(singleItem);

                    try
                    {
                        var cheapestItem = whereToBuy.GetCheapestPriceByEan(ean,"");
                        bool fixPrice = cheapestItem.fixedPrice == "04" ? true : false;

                        ProductSize productMeasurements = getProductMeasurments(productSizesDB,
                                                                                ean,
                                                                                packingList,
                                                                                cheapestItem);

                        var shippingCostVariation = new ShippingCostFinder(productMeasurements,
                                                                            singleItem.GetItemPrice(),
                                                                            fixPrice)
                                                                            .getShippingVariation();

                        Shipping shipping = new Shipping(shippingCostVariation.name,
                                                         shippingCostVariation.price,
                                                         conditionSet.Profit.ItemHandlingCosts,
                                                         productMeasurements.packing.netPrice);

                        var product = new Product(singleItem,
                                                    conditionSet,
                                                    cheapestItem,
                                                    productMeasurements,
                                                    shippingCostVariation.price);

                        double overAllProfit = product.getProfit(singleItem.GetItemPrice(), 3);
                        CalculatedProfit calculatedProfitEntity = new CalculatedProfit(agentName, cheapestItem, singleItem, category, productMeasurements, conditionSet, shipping, overAllProfit);

                        SendToDatabase(singleItem, calculatedProfitEntity);
                    }
                    catch (Exception ex)
                    {
                        appStatus.AddMessagLine(ex.Message);
                        if (ex is NoProductFoundException || ex is ItemNotAvailableException)
                        {
                            Shipping shipping = new Shipping("not available", 0, 0, 0);
                            BuyPriceRow emptyBuyPriceRow = new BuyPriceRow(singleItem.SellerSKU.ToString());
                            CalculatedProfit calculatedProfit = new CalculatedProfit(agentName, emptyBuyPriceRow, singleItem, category, new ProductSize(), conditionSet, shipping, -3.33);
                            SendToDatabase(singleItem, calculatedProfit);
                        }
                    }
                }
                appStatus.Successful();
            }
        }

        private static ConditionSet GetConditionSet(string category)
        {
            ProfitConditions defaultProfitConditions = new ProfitConditions(0, 1, 0.3);
            ContractConditions contractconditions = new ContractConditions(0, 0, 0);
            PlatformConditions platformConditionCategory = PlatformConditionByCategory.GetPlatformConditionsFromCategory(category, new PlatformConditions(15, 0, 0));
            return new ConditionSet(defaultProfitConditions, contractconditions, platformConditionCategory);
        }

        private static void SendToDatabase(AmazonOrder singleItem, CalculatedProfit calculatedProfit)
        {
           // insertToDb(calculatedProfit);
            //singleOrdersDB.collection.Update(Query.EQ("Order.AmazonOrderId", singleItem.AmazonOrderId), Update.Set("plus.OrderProcessed", DateTime.Now));
        }

        private static List<AmazonOrder> GetSingleOrderItems()
        {
            var orders = singleOrdersDB.GetOrders();
            Console.WriteLine("Number of items that need to be calculated: " + orders.Count().ToString());

            var singleOrder = orders.Where(x => x.Order.ASIN.IsString).ToList();
            var multiorders = orders.Where(x => x.Order.ASIN.IsBsonArray).ToList();

            var totalOrdersTemp = singleOrder.Select(x => x.Order).ToList();
            totalOrdersTemp.AddRange(SplitMultiOrders(multiorders));

            List<AmazonOrder> totalOrders = SplitQuantity(totalOrdersTemp);
            return totalOrders;
        }


        private static List<AmazonOrder> SplitQuantity(List<AmazonOrder> totalOrdersTemp)
        {
            List<AmazonOrder> totalOrders = new List<AmazonOrder>();
            foreach (var order in totalOrdersTemp)
            {
                try
                {
                    int quantity = int.Parse(order.QuantityOrdered.AsString);
                    var newItemPrice = double.Parse(order.ItemPrice["Amount"].ToString(), CultureInfo.InvariantCulture) / quantity;

                    for (int i = 0; i < quantity; i++)
                    {
                        var orderClone = (AmazonOrder)order.Clone();
                        orderClone.QuantityOrdered = 1;
                        orderClone.ItemPrice = newItemPrice;

                        totalOrders.Add(orderClone);
                    }
                }
                catch { }
            }

            return totalOrders;
        }

        private static List<AmazonOrder> SplitMultiOrders(List<SingleOrderItem> multiorders)
        {
            List<AmazonOrder> totalOrdersTemp = new List<AmazonOrder>();
            foreach (var multiOrder in multiorders)
            {
                int count = multiOrder.Order.ASIN.AsBsonArray.Count();

                AmazonOrder amazonOrder = multiOrder.Order;
                for (int i = 0; i < count; i++)
                {
                    try
                    {
                        AmazonOrder amazonOrderTemplate = (AmazonOrder)multiOrder.Order.Clone();

                        #region

                        amazonOrderTemplate.ASIN = multiOrder.Order.ASIN.AsBsonArray[i];
                        amazonOrderTemplate.SellerSKU = multiOrder.Order.SellerSKU.AsBsonArray[i];
                        amazonOrderTemplate.OrderItemId = multiOrder.Order.OrderItemId.AsBsonArray[i];
                        amazonOrderTemplate.Title = multiOrder.Order.Title.AsBsonArray[i];
                        amazonOrderTemplate.QuantityOrdered = multiOrder.Order.QuantityOrdered.AsBsonArray[i];
                        amazonOrderTemplate.QuantityShipped = multiOrder.Order.QuantityShipped.AsBsonArray[i];
                        amazonOrderTemplate.ItemPrice = multiOrder.Order.ItemPrice.AsBsonValue[i];
                        amazonOrderTemplate.ShippingPrice = multiOrder.Order.ShippingPrice.AsBsonValue[i];
                        amazonOrderTemplate.GiftWrapPrice = multiOrder.Order.GiftWrapPrice.AsBsonValue[i];
                        amazonOrderTemplate.ItemTax = multiOrder.Order.ItemTax.AsBsonValue[i];
                        amazonOrderTemplate.ShippingTax = multiOrder.Order.ShippingTax.AsBsonValue[i];
                        amazonOrderTemplate.GiftWrapTax = multiOrder.Order.GiftWrapTax.AsBsonValue[i];
                        amazonOrderTemplate.ShippingDiscount = multiOrder.Order.ShippingDiscount.AsBsonValue[i];
                        amazonOrderTemplate.PromotionDiscount = multiOrder.Order.PromotionDiscount.AsBsonValue[i];
                        amazonOrderTemplate.ConditionNote = multiOrder.Order.ConditionNote.AsBsonValue[i];
                        amazonOrderTemplate.ConditionId = multiOrder.Order.ConditionId.AsBsonValue[i];
                        amazonOrderTemplate.ConditionSubtypeId = multiOrder.Order.ConditionSubtypeId.AsBsonValue[i];

                        #endregion

                        totalOrdersTemp.Add(amazonOrderTemplate);
                    }
                    catch { }
                }
            }
            return totalOrdersTemp;
        }

        private static string GetCategoryFromAsin(AmazonOrder singleItem)
        {
            string categoryName = "empty_category";
            try
            {
                categoryName = categoriesDB.collection.FindOne(Query.EQ("_id", singleItem.ASIN.AsString)).category.ToString();
            }
            catch { }

            return categoryName;
        }

        private static string GetAgentName(AmazonOrder singleItem)
        {
            string agentName = string.Empty;
            try
            {
                if (!string.IsNullOrWhiteSpace(singleItem.AgentId))
                {
                    int agentId = 0;
                    int.TryParse(singleItem.AgentId, out agentId);
                    agentName = new Agent(agentId).agentEntity.agent.agentname;
                }
            }
            catch
            {
                agentName = "agent not found";
            }
            return agentName;
        }



        private static ProductSize getProductMeasurments(ProductSizesMeasurementsDB productSizesDB, string ean, PriceCalculation.PackingList packingList, BuyPriceRow cheapestItem)
        {
            int length;
            int width;
            int height;
            int weight;
            ProductSize productmeasurements;

            length = 0;
            width = 0;
            height = 0;
            weight = 0;

            if (cheapestItem.simpleSize.isValid)
            {
                length = cheapestItem.simpleSize.Length;
                width = cheapestItem.simpleSize.Width;
                height = cheapestItem.simpleSize.Height;
                weight = cheapestItem.simpleSize.Weight;
            }
            else
            {
                var productsizesFromDb = productSizesDB.collection.FindOne(Query.EQ("ean", ean));
                if (productsizesFromDb != null)
                {
                    length = productsizesFromDb.measure.length;
                    width = productsizesFromDb.measure.width;
                    height = productsizesFromDb.measure.height;
                    weight = productsizesFromDb.measure.weight;
                }
            }

            productmeasurements = new ProductSize(length.ToString(),
                               width.ToString(),
                              height.ToString(),
                              weight.ToString(),
                                    packingList);
            return productmeasurements;
        }

        public static void insertToDb(CalculatedProfit cpe)
        {
            CalculatedProfitDB cpdb = new CalculatedProfitDB();
            cpdb.collection.Insert(cpe);
        }
    }
}
