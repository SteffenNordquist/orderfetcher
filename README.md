# README #

This README would normally document whatever steps are necessary to get your application up and running.

### GetAmazonOrders ###

* Get all orders from amazon.
* Version 1

### How do I get set up? ###

* Double check local agent ID. Pull all updated DLL including mongodb. Deploy it in agent servers.(C:\DevNetworks\Amazon\GetAmazonOrders)
* Configuration [ GetAmazonOrders.exe.config ]
* Dependencies - [ DN.Classes, ProcessProfitOfOrder, mongodb ]
* Bug/Errors and possible solution : http://136.243.19.216:88/index.php?title=AmazonGetOrders

### ProcessProfitOfSplittedOrders ###

* Calculate all orders.
* Version 1

### How do I get set up? ###

* Pull all updated DLL including mongodb. Check the testing methods and database connection. Deploy it in 144 Server.(C:\DevNetworks\Amazon\AmazonProcessProfitOfOrder) 
* Configuration [ ProcessProfitOfSplittedOrders.exe.config ]
* Dependencies - [ DN.Classes, ProcessProfitOfOrder, mongodb, ProductDB, EbayOrder, WhereToBuyDLL, PriceCalculation(profitcalc) ]
* Bug/Errors and possible solution : http://136.243.19.216:88/index.php?title=AmazonProcessProfit



### WhereToBuyDLL ###

* Find the cheapest order from all of our databases.
* Version 1

### How do I get set up? ###

* reference this dll to applications
* Dependencies - [ productdb, dnclasses, mongodb , pricecalculation , newtonsoft]

### InsertOrderXmlToMongoDb ###

* Insert the order from amazon to our database
* Version 1

### How do I get set up? ###

* reference this dll to applications
* Dependencies - [ dnclasses, mongodb , newtonsoft]

### WhereToBuyTester ###

* Test order if it is available or not.
* Version 1

### How do I get set up? ###

* reference this dll to applications
* Dependencies - [ dnclasses, mongodb , WhereToBuyDLL]


### EbayOrderManagement ###

* Get the orders from ebay
* Version 1

### How do I get set up? ###

* Double check local agent ID. Pull all updated DLL including mongodb. Deploy it in agent servers.
* Dependencies - [ DN.Classes, EbayService, mongodb ]
* Bug/Errors and possible solution : http://136.243.19.216:88/index.php?title=AmazonGetOrders



### Who do I talk to? ###

* Rudy
* Other community or team contact