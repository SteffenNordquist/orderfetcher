﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using eBay.Service.Call;
using eBay.Service.Core.Sdk;
using eBay.Service.Util;
using eBay.Service.Core.Soap;


namespace EbayOrders
{
    public class EbayOrder
    {

        ApiContext context = new ApiContext();

        public EbayOrder(string ebayToken)
        {
            context.ApiCredential.eBayToken = ebayToken;
        }


        public List<OrderType> GetOrders(DateTime fromDateTime)
        {
            List<OrderType> orders = new List<OrderType>();

            //set the server url
            context.SoapApiServerUrl = "https://api.ebay.com/wsapi";

            //enable logging
            context.ApiLogManager = new ApiLogManager();
            context.ApiLogManager.ApiLoggerList.Add(new FileLogger("log.txt", true, true, true));
            context.ApiLogManager.EnableLogging = true;

            //set the version
            context.Version = "871";
            context.Site = SiteCodeType.Germany;
            // bool blnHasMore = true; // not used

            DateTime CreateTimeTo;

            GetOrdersCall getOrders = new GetOrdersCall(context);
            getOrders.DetailLevelList = new DetailLevelCodeTypeCollection();
            getOrders.DetailLevelList.Add(DetailLevelCodeType.ReturnAll);

            //CreateTimeTo set to the current time
            CreateTimeTo = DateTime.Now.ToUniversalTime();

            ////Assumption call is made every 15 sec. So CreateTimeFrom of last call was 15 mins
            ////prior to now
            //TimeSpan ts1 = new TimeSpan(9000000000);
            //CreateTimeFromPrev = CreateTimeTo.Subtract(ts1);

            ////Set the CreateTimeFrom the last time you made the call minus 2 minutes
            ////TimeSpan ts2 = new TimeSpan(1200000000);
            //TimeSpan ts2 = TimeSpan.FromDays(1);
            //CreateTimeFrom = CreateTimeFromPrev.Subtract(ts2);

            getOrders.CreateTimeFrom = fromDateTime;
            getOrders.CreateTimeTo = DateTime.Now;
            getOrders.Pagination = new PaginationType();
            getOrders.Pagination.EntriesPerPage = 1000;
            getOrders.IncludeFinalValueFee = true;
            getOrders.OrderStatus = OrderStatusCodeType.Completed;

            getOrders.OrderRole = TradingRoleCodeType.Seller;
            try
            {
                getOrders.Execute();

                if (getOrders.ApiResponse.Ack != AckCodeType.Failure)
                {
                    //Check if any orders are returned
                    if (getOrders.ApiResponse.OrderArray.Count != 0)
                    {
                        foreach (OrderType order in getOrders.ApiResponse.OrderArray)
                        {
							//var items = getOrders.ApiResponse.OrderArray.Count();
                            var counthis = getOrders.ApiResponse.OrderArray.ToArray().Count();
                            //Update your system with the order information.
                            Console.WriteLine("Order Number: " + order.OrderID);
                            Console.WriteLine("OrderStatus: " + order.OrderStatus);
                            Console.WriteLine("Order Created On: " + order.CreatedTime);


                            if (order.CheckoutStatus.Status == CompleteStatusCodeType.Complete)
                            {
								if (!orders.Select(x => x.OrderID.ToString()).Contains(order.OrderID))
								{
									orders.Add(order);
								}
								else
								{
									Console.WriteLine("Order found");
								}
                            }

                            Console.WriteLine("********************************************************");
                        }
                    }
                }
                else { Console.WriteLine("Return Failed"); }
            }

            catch { }
            return orders;

        }





        public List<TransactionType> GetTransactions(DateTime fromDateTime)
        {
            List<TransactionType> transactions = new List<TransactionType>();

            //set the server url
            context.SoapApiServerUrl = "https://api.ebay.com/wsapi";

            //enable logging
            context.ApiLogManager = new ApiLogManager();
            context.ApiLogManager.ApiLoggerList.Add(new FileLogger("log.txt", true, true, true));
            context.ApiLogManager.EnableLogging = true;

            //set the version
            context.Version = "863";
            context.Site = SiteCodeType.Germany;
            // bool blnHasMore = true; // not used

            DateTime CreateTimeTo;




            GetSellerTransactionsCall getTransactionTypes = new GetSellerTransactionsCall(context);
            getTransactionTypes.DetailLevelList = new DetailLevelCodeTypeCollection();
            getTransactionTypes.DetailLevelList.Add(DetailLevelCodeType.ReturnAll);



            //CreateTimeTo set to the current time
            CreateTimeTo = DateTime.Now.ToUniversalTime();

            ////Assumption call is made every 15 sec. So CreateTimeFrom of last call was 15 mins
            ////prior to now
            //TimeSpan ts1 = new TimeSpan(9000000000);
            //CreateTimeFromPrev = CreateTimeTo.Subtract(ts1);

            ////Set the CreateTimeFrom the last time you made the call minus 2 minutes
            ////TimeSpan ts2 = new TimeSpan(1200000000);
            //TimeSpan ts2 = TimeSpan.FromDays(1);
            //CreateTimeFrom = CreateTimeFromPrev.Subtract(ts2);



            getTransactionTypes.Execute();

            if (getTransactionTypes.ApiResponse.Ack != AckCodeType.Failure)
            {
                //Check if any orders are returned
                if (getTransactionTypes.ApiResponse.TransactionArray.Count != 0)
                {
                    foreach (TransactionType transactionType in getTransactionTypes.ApiResponse.TransactionArray)
                    {
                        //Update your system with the order information.
                        Console.WriteLine(transactionType.FinalValueFee);

                        transactions.Add(transactionType);

                        //if (transactionType.tus.Status == CompleteStatusCodeType.Complete)
                        //{

                        //    orders.Add(order);

                        //}

                        Console.WriteLine("********************************************************");
                    }
                }
            }
            else { Console.WriteLine("Return Failed"); }

            return transactions;

        }
    }
}
