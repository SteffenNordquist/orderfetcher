﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
namespace EbayOrderDB
{
    public class AgentEntity
    {
        public ObjectId Id { get; set; }
        public BsonDocument agent { get; set; }

        public string GetAgentLogoURL()
        {
            string picData = "";
            try
            {
                picData = agent["PictureDataAsString"].AsString;
            }
            catch { }
            return picData;
        }

        internal string GetFinancialAuthority()
        {
            try
            {
                return agent["financialauthorities"].AsString;
            }
            catch
            {
                return "";
            }
        }
    }
}
