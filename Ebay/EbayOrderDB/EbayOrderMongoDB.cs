﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using eBay.Service.Call;
using eBay.Service.Core.Sdk;
using eBay.Service.Util;
using eBay.Service.Core.Soap;


//using OrderData;
using InvoiceGenerator;

using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.IO;

using Newtonsoft.Json;
using System.Xml;
using System.Globalization;

//using OrderData;


//using MongoDB.Driver.GridFS;
namespace EbayOrderDB
{
	public class EbayOrderMongoDB
	{
		public MongoCollection<EbayOrderEntity> ebayOrderCollection;
		public MongoCollection<EbayOrderEntityBSON> ebayOrderCollectionBSON;
		public MongoCollection<AgentEntity> AgentCollection;
		public MongoCollection<InvoiceData> InvoiceDataCollection;
		public MongoDatabase Database;
		public MongoCollection<EbayOrderEntity> ebayOrderNotAvailable;

		private string EbayEmail = "";
		private int agentid;
		private string orderstatus = "";

		public EbayOrderMongoDB()
		{
			this.Database = new MongoClient(Properties.Settings.Default.ServerIp).GetServer().GetDatabase("Orders");
			this.ebayOrderCollection = Database.GetCollection<EbayOrderEntity>("EbaySingleOrderItems");
			this.ebayOrderNotAvailable = Database.GetCollection<EbayOrderEntity>("EbayUnavailableOrderItems");
		}

		public EbayOrderMongoDB(string ebayEmail, int agentid, string unpaid_status)
		{
			SetCollections();

			this.agentid = agentid;
			this.EbayEmail = ebayEmail;
			if (unpaid_status != string.Empty)
			{
				this.orderstatus = "_" + unpaid_status;
			}

		}



		public MongoCollection<EbayOrderEntity> SetCollections()
		{
			//Client = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/Orders");
			MongoDatabase Database = new MongoClient(Properties.Settings.Default.ServerIp).GetServer().GetDatabase("Orders");
			ebayOrderCollection = Database.GetCollection<EbayOrderEntity>("EbaySingleOrderItems");
			return ebayOrderCollection;
			#region Prev Code
			//ebayOrderCollectionBSON = Database.GetCollection<EbayOrderEntityBSON>("ebayorders_spartak" + orderstatus);
			//AgentCollection = Database.GetCollection<AgentEntity>("agents");
			//InvoiceDataCollection = Database.GetCollection<InvoiceData>("invoices_spartak2" + orderstatus);

			#endregion
		}

		public void InsertOrder(OrderType order, int AgentID)
		{
			EbayOrderEntity orderEntity = new EbayOrderEntity(order);

			string sku = order.TransactionArray[0].Item.SKU;

			if (sku == null || !sku.Contains("INF"))
			{
				var serialized = Newtonsoft.Json.JsonConvert.SerializeObject(new { order = orderEntity.order });
				// var addField = additionalField(serialized.Split('{'), AgentID);
				MongoDB.Bson.BsonDocument ebayOrder = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(serialized);

				var ebayOrderUpdate = Update.Set("order", ebayOrder["order"])
											.Set("plus.AgentID", AgentID);
				ebayOrderCollection.Update(Query.EQ("order.OrderID", order.OrderID), ebayOrderUpdate, UpdateFlags.Upsert);

				#region Prev Codes
				//  var update1 = Update.Set("order", orderEntity.order.ToBsonDocument());
				// ebayOrderCollection.Update(Query.EQ("order.OrderID", order.OrderID), update1, UpdateFlags.Upsert);

				//   EbayOrder ebayorder = new EbayOrder { order = ebayOrder };

				//var ebayinvoice = InvoiceDataCollection.FindOne(Query.EQ("reference", order.OrderID));

				//if (ebayinvoice == null)
				//{
				//    InsertToInvoiceData(ebayOrder);
				//}
				//else
				//{
				//    if (ebayinvoice.order.ShippedTime.Year < 2000)
				//    {
				//        var update2 = Update.Set("order.ShippedTime", ebayorder.GetShippedTime());
				//        InvoiceDataCollection.Update(Query.EQ("reference", order.OrderID), update2);
				//    }

				//    //to replace all with '80' countrycode
				//    var update = Update.Set("order.shippingAddress", ebayorder.GetCustomer().ToBsonDocument());
				//    InvoiceDataCollection.Update(Query.EQ("reference", order.OrderID), update);

				//}

				#endregion
			}
		}

		public DateTime getLatestOrderDateTime()
		{
			try
			{
				var result = ebayOrderCollectionBSON.Find(Query.EQ("order.SellerEmail", EbayEmail)).SetSortOrder(SortBy.Descending("order.CreatedTime")).First();

				var time = result.order["CreatedTime", DateTime.Now.AddDays(-40)].ToUniversalTime();
				return time;
			}
			catch
			{ }


			return DateTime.Now.AddDays(-40);


		}

		public string additionalField(string[] serializedString, int agentId)
		{
			string agent = "\"AgentID\":\"" + agentId.ToString() + "\",";
			string value = "";
			int count = 0;

			foreach (string str in serializedString)
			{
				string idea = "";
				if (count != 0)
				{
					idea = str;
					if (count == 2)
					{
						idea = "";
						idea = agent + str;
					}
					value = value + "{" + idea;

				}

				count++;
			}
			return value;
		}


		#region Insert to invoice collection

		public void InsertToInvoiceData(BsonDocument orderdetail)
		{
			/// modified by Ydur

			//EbayOrder order = new EbayOrder { order = orderdetail };

			//string agentName = order.GetAgentName();
			//string orderid = order.GetPlatformOrderId();
			//int invoiceID = InvoiceID("Ebay");
			////try
			////{

			//Order newOrder = new Order
			//{
			//	orderItemList = order.GetOrderItems(),
			//	invoiceId = invoiceID,
			//	amazonOrderId = orderid,
			//	shipping = order.GetTotalShipping(),
			//	shippingAddress = order.GetCustomer(),
			//	billingAddress = order.GetBillingAddress(),
			//	paymentType = "bezahlt",
			//	orderedFromPlatform = "Ebay",
			//	purchasedDate = order.GetPurchaseDate(),
			//	PaidTime = order.GetPaidTime(),
			//	ShippedTime = order.GetShippedTime(),
			//	LastChangeTime = order.GetLastChangeTime(),
			//	sellerFee = order.GetSellerFee(),
			//	moneyTransactionFee = order.GetMoneyTransactionFee()

			//};
			//var agent = new Agent();

			//UpdateBuilder update = Update.Set("order", newOrder.ToBsonDocument()).Set("agent", agent.ToBsonDocument());
			//InvoiceDataCollection.Update(Query.EQ("reference", orderid), update, UpdateFlags.Upsert);

			//UpdateOrderItem(orderid, invoiceID);

			//Console.WriteLine(newOrder.shippingAddress.ToJson().ToString());   


			//}
			//catch
			//{
			//    Console.WriteLine("ERROR IMPORTING");
			//}

		}

		//Agent GetAgent(string email)
		//{
		//    var agentFromDB = AgentCollection.FindOne(Query.EQ("agent.email", email));
		//    if (agentFromDB != null)
		//    {
		//        string agentLogoURL = agentFromDB.GetAgentLogoURL();

		//        if (agentLogoURL != "")
		//        {
		//            agentLogoURL = "http://148.251.0.235/AmazonOrder/AmazonOrders/Images" + agentLogoURL.Replace("~/images/", "/");
		//        }

		//        Agent agent = new Agent
		//        {
		//            agentID = agentFromDB.agent["agentId"].AsInt32,
		//            agentLogoFilePath = agentLogoURL,
		//            sendBackAddress = "Versandlager für Bücher und Medien|Dorotheenstraße 30, 10117 Berlin",
		//            companyName = email,
		//            financialAuthorityString = agentFromDB.GetFinancialAuthority()
		//        };
		//        agent.address = new Address(email, "", agentFromDB.agent["address1"].AsString, agentFromDB.agent["address2"].AsString, agentFromDB.agent["postcode"].AsString, agentFromDB.agent["city"].AsString, "", "", "");

		//        return agent;
		//    }

		//    return null;
		//}

		int InvoiceID(string platform)
		{
			int invoiceID = 0;

			if (InvoiceDataCollection.Find(Query.And(Query.EQ("agent.agentID", agentid), Query.EQ("order.orderedFromPlatform", platform))).Count() > 0)
			{
				var max = InvoiceDataCollection.Find(Query.And(Query.EQ("agent.agentID", agentid), Query.EQ("order.orderedFromPlatform", platform))).SetSortOrder(SortBy.Descending("order.invoiceId")).SetLimit(1).FirstOrDefault();
				invoiceID = Convert.ToInt32(max.order.invoiceId) + 1;
			}


			return invoiceID;
		}

		void UpdateOrderItem(string id, int invoiceID)
		{
			UpdateBuilder update = MongoDB.Driver.Builders.Update.Set("invoiceid", invoiceID);
			ebayOrderCollection.Update(Query.EQ("order.OrderID", id), update);
		}

		#endregion


		public List<EbayOrderEntity> GetOrders(IMongoQuery query)
		{
			//return SetCollections().Find(query).ToList();

			return SetCollections().Find(query).ToList();

		}

		public List<EbayOrderEntity> GetUnavailableOrders(IMongoQuery query)
		{
			//return SetCollections().Find(query).ToList();
			MongoClient Client;
			MongoServer Server;
			MongoDatabase Database;
			string supplier;
			MongoCollection<BsonDocument> SupplierCollection;
			List<string> mainAsins = new List<string>();

			Client = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/Orders");
			Server = Client.GetServer();
			Database = Server.GetDatabase("Orders");
			SupplierCollection = Database.GetCollection<BsonDocument>("EbayUnavailableOrderItems");


			// var findthis = SupplierCollection.FindAll().ToList();


			return ebayOrderNotAvailable.FindAll().ToList();

		}

		private bool filter(EbayOrderEntity x)
		{
			bool identifier = false;
			DateTime createdTime = new DateTime();

			if (x.order.CreatedTime.ToString().Contains("."))
			{
				createdTime = DateTime.Parse(x.order.CreatedTime.ToString(), new CultureInfo("de-DE"));
			}
			else
			{
				createdTime = DateTime.ParseExact(x.order.CreatedTime.ToString(), "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
			}

			if (createdTime.Date >= DateTime.Now.AddDays(-5).Date)
			{
				Console.WriteLine("DateTime " + createdTime.Month + "  " + createdTime.Day);
				identifier = true;
			}


			return identifier;
		}


	}
}
