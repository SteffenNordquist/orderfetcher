﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using eBay.Service.Call;
using eBay.Service.Core.Sdk;
using eBay.Service.Util;
using eBay.Service.Core.Soap;

using MongoDB.Bson;

namespace EbayOrderDB
{
    public class EbayOrderEntityBSON
    {
        public ObjectId id { get; set; }
        public BsonDocument order { get; set; }
        //public int invoiceid { get; set; }
    }
}
