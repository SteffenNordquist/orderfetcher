﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using eBay.Service.Call;
using eBay.Service.Core.Sdk;
using eBay.Service.Util;
using eBay.Service.Core.Soap;

using EbayOrders;

using EbayOrderDB;



using System.IO;

using System.Threading;
using DN_Classes.AppStatus;

namespace EbayOrderManagement
{
	class Program
	{
		static string ebaytoken = "";
		static string ebayemail = "";


		static void Main(string[] args)
		{

			do
			{
				DN_Classes.Agent.Agent agent = new DN_Classes.Agent.Agent();
				using (ApplicationStatus appStatus = new ApplicationStatus("GetEbayOrders_" + agent.agentEntity.agent.agentname + " " + agent.agentEntity.agent.agentID.ToString()))
				{
					try
					{
						runMain(agent);
						appStatus.Successful();
						appStatus.Dispose();
					}
					catch (Exception ex)
					{
						appStatus.AddMessagLine(ex.Message);
					}

					if (Properties.Settings.Default.Loop)
					{
						Thread.Sleep(TimeSpan.FromMinutes(2));
					}
				}

			} while (Properties.Settings.Default.Loop);

		}


		static void runMain(DN_Classes.Agent.Agent agent)
		{


			if (agent != null)
			{
				ebaytoken = agent.agentEntity.agent.keys.ebay.token;
				ebayemail = agent.agentEntity.agent.keys.ebay.email;


				EbayOrder ebayOrder = new EbayOrder(ebaytoken);

				EbayOrderMongoDB ebayMongoDb = new EbayOrderMongoDB(ebayemail, agent.agentEntity.agent.agentID, "");

				//var t = ebayMongoDb.getLatestOrderDateTime().AddDays(-40);
				var timeFrom = DateTime.Now.AddDays(Properties.Settings.Default.DaysTimeFrom);

				List<OrderType> orderTypeList = new List<OrderType>();

				DateTime currrentDateTime = timeFrom;

				Console.WriteLine("orders fetching time limit " + DateTime.Now.AddDays(-1).ToString());
				if (timeFrom < DateTime.Now.AddDays(-1))
				{
					try
					{
						timeFrom = orderTypeList.OrderByDescending(x => x.CreatedTime).First().CreatedTime;
					}
					catch
					{
						// ignored
					}

					var orders2 = ebayOrder.GetOrders(timeFrom);
					orderTypeList.AddRange(orders2);

				}

				foreach (OrderType order in orderTypeList)
				{
					Console.WriteLine("inserting/updating... " + order.OrderID);
					ebayMongoDb.InsertOrder(order, agent.agentEntity.agent.agentID);
					Thread.Sleep(2000);
				}
				Console.WriteLine("********************************************************");
				Console.WriteLine("********************************************************");
				Console.WriteLine("Sleeping for 2 minutes..");
			}

		}
	}
}
