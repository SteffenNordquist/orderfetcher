﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadWriteRegisterKey
{
    public static class Booklooker
    {
        public static string username = "";
        public static string password = "";

        public static void Register(string username, string password)
        {
            Reg.Write("Booklooker", "Username", username);
            Reg.Write("Booklooker", "Password", password);
        }

        public static bool HasKeyValues()
        {
            List<KeyAndValue> list = Reg.Get("Booklooker");
            bool hasOneEmpty = false;
            foreach (KeyAndValue k in list)
            {
                if (k.value.Count() < 1)
                {
                    hasOneEmpty = true;
                    break;
                }

                if (k.keyname.ToLower() == "username")
                {
                    username = k.value;
                }
                else if (k.keyname.ToLower() == "password")
                {
                    password = k.value;
                }
            }

            if (list.Count() < 1 || hasOneEmpty)
            {
                return false;
            }
            else
            {
                
                return true;
            }
        }
    }
}
