﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;
using System.IO;
using System.Xml;
using System.Xml.Linq;
namespace ReadWriteRegisterKey
{
    public class Reg
    {
        
        static string fallbackDir = @"C:\temp\";

        public static List<KeyAndValue> Get(string subKey)
        {
            List<KeyAndValue> kandv = new List<KeyAndValue>();
            try
            {
                RegistryKey rk = Registry.LocalMachine.OpenSubKey("Software", true);
                RegistryKey rk2 = rk.OpenSubKey(subKey);
                if (rk2 == null)
                {
                    rk.CreateSubKey(subKey);
                }
                rk2 = rk.OpenSubKey(subKey, true);

                String[] names = rk2.GetValueNames();

                foreach (String s in names)
                {
                    string value = "";
                    try
                    {
                        value = rk2.GetValue(s).ToString();
                    }
                    catch { }

                    if (value != "")
                    {
                        kandv.Add(new KeyAndValue { keyname = s, value = value });
                    }

                }
            }
            catch
            {
                CreateWorkingFolders();
                kandv = GetValuesFromXML(subKey);

            }

            return kandv;
        }

        public static void Write(string subKey, string KeyName, string Value)
        {
            try
            {
                RegistryKey rk = Registry.LocalMachine.OpenSubKey("Software", true);
                RegistryKey rk2 = rk.OpenSubKey(subKey, true);
                if (rk2 == null)
                {
                    rk.CreateSubKey(subKey);
                }
                rk2 = rk.OpenSubKey(subKey, true);
                rk2.SetValue(KeyName.ToUpper(), Value);
            }
            catch
            {
                string xmlDir = fallbackDir + "Filekeys.xml";
                XmlDocument doc = new XmlDocument();
                if (!File.Exists(xmlDir))
                {
                    
                    XmlElement el = doc.CreateElement(string.Empty, "Filekeys", string.Empty);
                    doc.AppendChild(el);
                    doc.Save(xmlDir);
                }

                doc.Load(xmlDir);
                XmlNode filekeys = doc.SelectSingleNode("//Filekeys");
                if (filekeys.SelectSingleNode("//"+subKey) == null)
                {
                    XmlNode sub = doc.CreateElement(string.Empty, subKey, string.Empty);
                    filekeys.AppendChild(sub);
                }

                XmlNode subkey = filekeys.SelectSingleNode("//" + subKey);
                if (subkey.SelectSingleNode("//" + KeyName) == null)
                {
                    XmlNode subnode = doc.CreateElement(string.Empty, KeyName, string.Empty);
                    subkey.AppendChild(subnode);
                }
                XmlNode node = subkey.SelectSingleNode("//" + KeyName);
                node.InnerText = "";
                XmlText text = doc.CreateTextNode(Value);
                node.AppendChild(text);

                doc.Save(xmlDir);

            }
            
        }



        private static void CreateWorkingFolders()
        {
            if (!new DirectoryInfo(fallbackDir).Exists)
            {
                new DirectoryInfo(fallbackDir).Create();
            }
        }



        static List<KeyAndValue> GetValuesFromXML(string subkey)
        {
            List<KeyAndValue> kandv = new List<KeyAndValue>();
            string xmlDir = fallbackDir + "Filekeys.xml";
            if (File.Exists(xmlDir))
            {
                try
                {
                    XmlDocument xml = new XmlDocument();
                    xml.Load(xmlDir);
                    if (subkey.ToLower() == "booklooker")
                    {
                        var username = xml.SelectSingleNode("//Booklooker/Username").InnerText;
                        var password = xml.SelectSingleNode("//Booklooker/Password").InnerText;
                        kandv.Add(new KeyAndValue { keyname = "Username", value = username });
                        kandv.Add(new KeyAndValue { keyname = "Password", value = password });
                    }
                    else if (subkey.ToLower() == "ebay")
                    {
                        var ebayemail = xml.SelectSingleNode("//Ebay/EbayEmail").InnerText;
                        var ebaytoken = xml.SelectSingleNode("//Ebay/EbayToken").InnerText;
                        kandv.Add(new KeyAndValue { keyname = "EbayEmail", value = ebayemail });
                        kandv.Add(new KeyAndValue { keyname = "EbayToken", value = ebaytoken });
                    }
                    else if (subkey.ToLower() == "amazon")
                    {
                        var accesskeyid = xml.SelectSingleNode("//Amazon/AccessKeyId").InnerText;
                        var secretaccesskeyid = xml.SelectSingleNode("//Amazon/SecretAccessKeyId").InnerText;
                        var merchantid = xml.SelectSingleNode("//Amazon/MerchantId").InnerText;
                        var marketplaceid = xml.SelectSingleNode("//Amazon/MarketPlaceId").InnerText;
                        kandv.Add(new KeyAndValue { keyname = "AccessKeyId", value = accesskeyid });
                        kandv.Add(new KeyAndValue { keyname = "SecretAccessKeyId", value = secretaccesskeyid });
                        kandv.Add(new KeyAndValue { keyname = "MerchantId", value = merchantid });
                        kandv.Add(new KeyAndValue { keyname = "MarketPlaceId", value = marketplaceid });
                    }
                }
                catch { }
            }

            return kandv;
        }
    }
}
