﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using eBay.Service.Call;
using eBay.Service.Core.Sdk;
using eBay.Service.Util;
using eBay.Service.Core.Soap;
using System.IO;
using ProcuctDB;
using ProcuctDB.Knv;
using ProcuctDB.Libri;


namespace EbayListings
{
    class Program
    {
        static void Main(string[] args)
        {
            IDatabase knv = new KnvDB(Properties.Settings.Default.KnvServerIp);
            IDatabase libri = new LibriDB();

            //IProduct libriProduct = libri.GetMongoProductByEan(""); 
            //IProduct knvProduct = knv.GetMongoProductByEan("");

           

            


            Dictionary<string, string> skuCatdict = new Dictionary<string, string>(); 

        ApiContext context = new ApiContext();
        //set the server url // https://api.ebay.com/ws/api.dll
            context.SoapApiServerUrl = "https://api.ebay.com/wsapi";
          //  context.SoapApiServerUrl = "https://api.ebay.com/ws/api.dll";

            //enable logging
            context.ApiLogManager = new ApiLogManager();
            context.ApiLogManager.ApiLoggerList.Add(new FileLogger("log.txt", true, true, true));
            context.ApiLogManager.EnableLogging = true;

            //set the version
            context.Version = "863";
            context.Site = SiteCodeType.Germany;

            

            context.ApiCredential.eBayToken = Properties.Settings.Default.EbayToken;
            context.ApiCredential.ApiAccount.Application = Properties.Settings.Default.ApiAccountApplication;
            context.ApiCredential.ApiAccount.Developer = Properties.Settings.Default.ApiAccountDeveloper;
            context.ApiCredential.ApiAccount.Certificate = Properties.Settings.Default.ApiAccountCertificate;

            context.ApiCredential.eBayAccount = new eBayAccount(Properties.Settings.Default.EbayAccount,""); 


            GetSellerListCall getSellerList = new GetSellerListCall(context);
            getSellerList.DetailLevelList = new DetailLevelCodeTypeCollection();
            getSellerList.DetailLevelList.Add(DetailLevelCodeType.ReturnHeaders);
            

            
            getSellerList.GranularityLevel = GranularityLevelCodeType.Coarse;
            getSellerList.IncludeVariations = false;
            getSellerList.UserID = Properties.Settings.Default.EbayAccount;
          //  getSellerList.Seller = new UserType();

            getSellerList.StartTimeFrom = DateTime.Now.AddDays(-7);
            getSellerList.StartTimeTo = DateTime.Now;
            getSellerList.Timeout = 1000 * 60;


            for (int i = 1; i < 20; i++)
            {
                getSellerList.Pagination = new PaginationType() { EntriesPerPage = 200, PageNumber = i, PageNumberSpecified = true, EntriesPerPageSpecified = true };

                ItemTypeCollection itc = null;

                while (itc == null)
                {
                    try
                    {
                        itc = getSellerList.GetSellerList();
                    }
                    catch
                    {
                    }
                }
                
                foreach (ItemType itemType in itc)
                {
                    string categoryName = "";
                    try
                    {
                        categoryName = itemType.PrimaryCategory.CategoryName;
                    }
                    catch { }

                    if (itemType.SKU != null)
                    {
                        try
                        {
                            skuCatdict.Add(itemType.SKU, categoryName);
                           // Console.WriteLine("{0} - {1}", itemType.SKU, categoryName);

                            string sku = itemType.SKU.Split('_')[0]; 
                            IProduct product = libri.GetMongoProductByEan(sku);

                            if (product == null)
                            {
                                product = knv.GetMongoProductByEan(sku);
                            }

                            if(product != null)
                                Console.WriteLine("{0} - height: {1}", sku, product.getSmallestNotZero());
                            else
                                Console.WriteLine("{0} - null", sku);
                        }
                        catch
                        {
                            Console.WriteLine("error adding");
                        }


                        ListingStatusCodeType listingStatus = itemType.SellingStatus.ListingStatus;

                        if (listingStatus == ListingStatusCodeType.Active)
                        {
                        }
                        else
                        {
                            Console.WriteLine("Angebot beendet");
                        }
                    }
                }
            }

            File.WriteAllLines("C:\\temp\\categories.txt", skuCatdict.Select(x => x.Key + "\t" + x.Value));
        }


    }



}
