﻿using eBay.Service.Core.Soap;
using EbayOrderDB;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Xml;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json.Linq;
using System.IO.Compression;
using ProcessProfitOfOrderDll;


namespace ProcessProfitOrders
{
	public class MultiOrders
	{
		public List<EbayOrderEntity> Orders = new List<EbayOrderEntity>();

		public MultiOrders(List<EbayOrderEntity> Orders)
		{
			this.Orders = Orders;
		}

		public List<EbayOrderEntity> SplitMultiOrders()
		{
			List<EbayOrderEntity> totalOrdersTemp = new List<EbayOrderEntity>();

			foreach (EbayOrderEntity multiOrder in Orders)
			{
				//var trans = multiOrder.order.TransactionArray.Cast<TransactionType>().ToList();
				List<TransactionType> trans = new List<TransactionType>();
				int countTrans = multiOrder.order.TransactionArray.Count;
				foreach (dynamic transaction in multiOrder.order.TransactionArray)
				{											   //EbayOrderEntity
					TransactionTypeCollection orderTrans = multiOrder.order.TransactionArray;
						 
					string output = JsonConvert.SerializeObject(transaction);

					TransactionType deserializedProduct = JsonConvert.DeserializeObject<TransactionType>(output);

					trans.Add(deserializedProduct);
				}

				multiOrder.order.TransactionArray.Clear();
				
			    
				//dynamic dymultiPlus = new ExpandoObject();
				//dymultiPlus = multiOrder.plus;
				////var convert = multiOrder.order.ToBsonDocument();
				////convert.Remove("_id");

				//string plusmultijson = JsonConvert.SerializeObject(dymultiPlus);
				//BsonDocument deserializedOrderPlus = JsonConvert.DeserializeObject<BsonDocument>(plusmultijson);
				foreach (TransactionType tran in trans)
				{
					multiOrder.order.TransactionArray.Clear();

						
					dynamic dymultiorder = new ExpandoObject();
					dymultiorder = multiOrder.order;

					string multijson = JsonConvert.SerializeObject(dymultiorder);
					//dynamic multi_obj = JsonConvert.DeserializeObject( multijson );
					////string tranjson = JsonConvert.SerializeObject(tran);
					////dynamic tran_obj = JsonConvert.DeserializeObject( tranjson );

					////multi_obj.TransactionArray = tran_obj;
					//multijson = JsonConvert.SerializeObject(multi_obj);

					OrderType deserializedOrder = JsonConvert.DeserializeObject<OrderType>(multijson);

					deserializedOrder.TransactionArray.Add(tran);
					//deserializedOrder.TransactionArray.Clear();

					//var to = new TotalOrders().getOrder(tran, deserializedOrder);

					EbayOrderEntity newOrder = new EbayOrderEntity { order = deserializedOrder, plus = multiOrder.plus };
					totalOrdersTemp.Add(newOrder);
				}

				//OrderType deserializedOrder = JsonConvert.DeserializeObject<OrderType>(multijson);
				#region
				//List<int> index = new List<int>();
				//int count= 0;
				//foreach (var trans in transList)
				//{

				//	totalOrdersTemp.Add(multiOrder);
				//	index.Add(count);
				//	count++;
				//}

				//count = 0;
				//foreach (var item in totalOrdersTemp)
				//{
				//	for (int i = 0; i < index.Count(); i++)
				//	{
				//		if (count != index[i])
				//		{
				//			item.order.TransactionArray.RemoveAt(i);
				//		}
				//	}
					
				//	count++;
				//}
				//int length = 0;
				//foreach (var anothetrans in transList)
				//{
				//	length++;
				//}

				//int outsidecount = 0;
				//foreach (var trans in transList)
				//{
				//	EbayOrderEntity ebayOrderTemplate = (EbayOrderEntity)multiOrder.Clone();

				//	EbayOrderEntity ebayOrderTemplate = 
				//	int count=0;
				//	for (int index = 0; index < length; index++ )
				//	{
				//		if (count != outsidecount)
				//		{
				//			ebayOrderTemplate.order.TransactionArray.RemoveAt(count);
				//		}
				//		count++;
				//	}

				//	outsidecount++;
				//	totalOrdersTemp.Add(ebayOrderTemplate);
				//}

				
				//foreach (var transac in transList)
				//{
																									
				//	try
				//	{
				//		
				//		//EbayOrderEntity ebayOrderTemplate = (EbayOrderEntity)multiOrder.Clone();

				//		//ebayOrderTemplate.order.TransactionArray.RemoveAt(1);

				//		//totalOrdersTemp.Add(ebayOrderTemplate);


				//		//EbayOrderEntity ebayOrderTemplate1 = (EbayOrderEntity)multiOrder.Clone();

				//		//ebayOrderTemplate1.order.TransactionArray.RemoveAt(0);

				//		//totalOrdersTemp.Add(ebayOrderTemplate);

				//		//EbayOrderEntity ebayOrderTemplate = new EbayOrderEntity();
				//		//ebayOrderTemplate.order = new OrderType();
				//		//ebayOrderTemplate.order.TransactionArray = new TransactionTypeCollection();

				//		//var newTrans = new TransactionTypeCollection(transList).ToArray();


						
				//		//var convert = multiOrder.order.ToBsonDocument();
				//		//convert.Remove("_id");


				//		//string json = convert.ToJson();

				//		//var jsonResult = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(json);

				//		//dynamic deserializedValue = JsonConvert.DeserializeObject(json);

				//		//var values = deserializedValue["Foo2"];
				//		//for (int i = 0; i < values.Count; i++)
				//		//{
				//		//	Console.WriteLine(values[i]["specific_field"]);
				//		//}

				//		//byte[] byteArray = Encoding.UTF8.GetBytes(json);
				//		//using (var stream = new MemoryStream(byteArray))
				//		//{
				//		//	Serializer.ReadObject(stream);
				//		//}

				//		//dynamic data = JObject.Parse(json);
				//		//var get = JsonConvert.DeserializeObject(json);
				//		//JObject o = JObject.Parse(json);
				//		//var get = o["TransactionArray"];
				//		//XmlDocument xmlDoc = JsonConvert.DeserializeXmlNode(json, "root");

				//		//var eanNode = xmlDoc.SelectSingleNode("//" + "TransactionArray");
						
						
				//		//TransactionType trans1 = new TransactionType();

				//		//trans1.ActualShippingCost = transac.ActualShippingCost;
				//		//trans1.AdjustmentAmount = transac.AdjustmentAmount;
				//		//trans1.AmountPaid = transac.AmountPaid;
				//		//trans1.Any = transac.Any;
				//		//trans1.BestOfferSale = transac.BestOfferSale;
				//		//trans1.BestOfferSaleSpecified = transac.BestOfferSaleSpecified;
				//		//trans1.BundlePurchase = transac.BundlePurchase;
				//		//trans1.BundlePurchaseSpecified = transac.BundlePurchaseSpecified;
				//		//trans1.Buyer = transac.Buyer;
				//		//trans1.BuyerCheckoutMessage = transac.BuyerCheckoutMessage;
				//		//trans1.BuyerGuaranteePrice = transac.BuyerGuaranteePrice;
				//		//trans1.BuyerMessage = transac.BuyerMessage;
				//		////trans1.BuyerPaidStatus = true;
				//		//trans1.BuyerPaidStatusSpecified = transac.BuyerPaidStatusSpecified;
				//		//trans1.CartID = transac.CartID;
				//		//trans1.CodiceFiscale = transac.CodiceFiscale;
				//		//trans1.ContainingOrder = transac.ContainingOrder;
				//		//trans1.ConvertedAdjustmentAmount = transac.ConvertedAdjustmentAmount;
				//		////trans1.ConvertedAmountPaid = true;
				//		//trans1.ConvertedTransactionPrice = transac.ConvertedTransactionPrice;
				//		//trans1.CreatedDate = transac.CreatedDate;
				//		////trans1.CreatedDateSpecified = DateTime.Now;
				//		//ebayOrderTemplate.order.TransactionArray.Add(trans1);
				//		

				//		////totalOrdersTemp.Add(ebayOrderTemplate);
				//	}
				//	catch {}
				//}
				#endregion


				double shippingCost = multiOrder.order.ShippingServiceSelected.ShippingServiceCost.Value / countTrans;


			}


			return totalOrdersTemp;
		}


		public List<EbayOrderEntity> SplitByQuantity(List<EbayOrderEntity> Orders)
		{
			List<EbayOrderEntity> totalOrders = new List<EbayOrderEntity>();

			foreach (var item in Orders)
			{
				try
				{
					int quantity = new GetFields(item.order).getQuantity(item);
					//double newItemPrice = item.order.Total.Value / quantity;
					
					for (int i = 0; i < quantity; i++)
					{

						var orderClone = (EbayOrderEntity)item.Clone();
						int countItems = orderClone.order.TransactionArray.Count + quantity;

						foreach (dynamic transaction in orderClone.order.TransactionArray)
						{
							
							transaction.QuantityPurchased = 1;

							//double sellPricePerItemQty = transaction.TransactionPrice.Value;

							////orderClone.order.Total.Value = sellPricePerItemQty;
						}

						totalOrders.Add(orderClone);
					}
				}
				catch { }
			}

			return totalOrders;

		}


	}
}
