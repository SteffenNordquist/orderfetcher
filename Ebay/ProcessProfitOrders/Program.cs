﻿using EbayOrderDB;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProcessProfitOfOrderDll;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProcessProfitOrders
{
	class Program
	{


		static void Main(string[] args)
		{
			//CultureInfo culture = new CultureInfo("de");
			//double itemprice = Double.Parse("2.900", culture);
			do
			{
				runMain();
				if (Properties.Settings.Default.Loop)
				{
					Console.WriteLine("Sleeping for 2 minutes...");
					Thread.Sleep(TimeSpan.FromMinutes(2));
				}
			} while (Properties.Settings.Default.Loop);
		}

		private static void runMain()
		{

			InsertAndUpdateToDatabase toDatabase = new InsertAndUpdateToDatabase();
			//updateBugs();
			//Thread.Sleep(100000000);
			//var chungkang = Console.ReadLine();
			var allorders = getAllOrders(Query.NotExists("plus.OrderProcessed"));


			Console.WriteLine("Number of items " + allorders.Count());

			MultiOrders splitMultiOrders = new MultiOrders(allorders);
			var multiOrders = splitMultiOrders.SplitMultiOrders();
			var totalOrders = new OrderQuantity().SplitQuantity(multiOrders);

			Console.WriteLine("Total Orders " + totalOrders.Count());
			var allCalculatedOrders = new ProcessOrders().ProcessAllItems(totalOrders);

			Console.WriteLine("Number of orders:" + allCalculatedOrders.Count());

			foreach (var singleorder in allCalculatedOrders)
			{
				Console.WriteLine(singleorder.Key.order.OrderID + "\t imported.");
				// Dont forget to edit this.
				toDatabase.ebaySendToDatabase(singleorder.Key,
										  singleorder.Value);

			}

			IMongoQuery notAvailableQuery = Query.And(Query.Exists("plus.DateIdentified"),
												  Query.LT("plus.NumberOfDays", 10));

			//var allunavailableorders = getAllUnavailableOrders(notAvailableQuery);

			//Console.WriteLine("Unavailable orders:" + allunavailableorders.Count());
			//var allUnavailableCalculatedOrders = new ProcessOrders().ProcessAllItems(allunavailableorders);

			//foreach (var order in allUnavailableCalculatedOrders)
			//{
			//	if (order.Value.Item.Profit != -3.33 && order.Value.Shipping.ShippingName != "not available")
			//	{
			//		Console.WriteLine(order.Key.order.OrderID + "\t updated to available.");
			//		toDatabase.updateFoundOrder(order.Key, order.Value);
			//	}
			//}
			//var unmultiOrders = splitMultiOrders.SplitMultiOrders(allunavailableorders);
			//var untotalOrders = new OrderQuantity().SplitQuantity(multiOrders).Where(x => filterIds(x, ids)).ToList();

			//List<AmazonOrder> totalUnavailableOrders = getSingleOrderItems.SplitOrderItems(getUnavailableOrders).Where(x=> filterIds(x,ids)).ToList();
		}

		private static void updateBugs()
		{


			List<string> sampleOrderId = new List<string>();
			var getAllFiles = File.ReadAllLines(@"C:\DevNetworks\bugs.txt").ToList().Select(x => x.Split('\t')[4]).ToList();

			//foreach (var file in getAllFiles)
			//{
			//	sampleOrderId.Add(file);
			//}

			sampleOrderId.Add("272199441033-1636477339017");
			sampleOrderId.Add("381591147534-703173635025");
			sampleOrderId.Add("381591219991-703183174025");
			//sampleOrderId.Add("381578098970-703034962025");
			//sampleOrderId.Add("381596973774-703028293025");
			//sampleOrderId.Add("121835254258-1595732924002");

			List<EbayOrderEntity> allOrders = new List<EbayOrderEntity>();
			int count = 0;
			Console.WriteLine("Number of orders: " + getAllFiles.Count());
			foreach (var item in sampleOrderId)
			{
				count++;
				Console.WriteLine(count);
				allOrders.AddRange(getAllOrders(Query.EQ("order.OrderID", item)));
			}

			MultiOrders splitMultiOrders = new MultiOrders(allOrders);

			var multiOrders = splitMultiOrders.SplitMultiOrders();
			var totalOrders = splitMultiOrders.SplitByQuantity(multiOrders);

			var allCalculatedOrders = new ProcessOrders().ProcessAllItems(totalOrders);

			foreach (var item in allCalculatedOrders)
			{
				if ((item.Value.Item.Profit != -3.33 && item.Value.Shipping.ShippingName != "not available"))
				{

					Console.WriteLine(item.Key.order.OrderID + "\t updated.");
					new InsertAndUpdateToDatabase().updateBugs(item.Key, item.Value);
				}
			}

			Console.WriteLine("Close the tool");
		}

		private static bool filterIds(EbayOrderEntity ebayOrder, Dictionary<string, List<string>> ids)
		{
			foreach (var item in ids)
			{
				if (item.Key == ebayOrder.order.OrderID)
				{
					string sellerSku = "";
					foreach (dynamic order in ebayOrder.order.TransactionArray)
					{
						sellerSku = order.Item.SKU;
						break;
					}

					return !item.Value.ToList().Contains(sellerSku);
				}
			}

			return true;
		}

		private static List<EbayOrderEntity> getAllOrders(IMongoQuery query)
		{
			var orders = new EbayOrderMongoDB().GetOrders(query);
			//allDocuments = collection2.Find(notAvailableQuery).Where(x => x.plus["DateIdentified"].AsDateTime.Date != DateTime.Now.Date && !x.plus["ItemAvailability"].AsBoolean ).ToList();

			return orders;
		}

		private static List<EbayOrderEntity> getAllUnavailableOrders(IMongoQuery query)
		{
			var orders = new EbayOrderMongoDB().GetOrders(query);


			//orders
			orders = orders.Where(x => x.plus["DateIdentified"].AsDateTime.Date != DateTime.Now.Date && !x.plus["ItemAvailability"].AsBoolean).ToList();

			return orders;
		}


		private static List<EbayOrderEntity> getSampleOrders(IMongoQuery query)
		{
			return new EbayOrderMongoDB().GetOrders(query);
		}

	}
}
