﻿using eBay.Service.Core.Soap;
using EbayOrderDB;
using ProcessProfitOfOrderDll;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessProfitOrders
{
    public class OrderQuantity
    {
        public List<EbayOrderEntity> SplitQuantity(List<EbayOrderEntity> totalOrdersTemp)
        {
            List<EbayOrderEntity> totalOrders = new List<EbayOrderEntity>();

            foreach (var item in totalOrdersTemp)
            {
                try
                {
					int quantity = new GetFields(item.order).getQuantity(item);
                  //double newItemPrice = item.order.Total.Value / quantity;
					  
                    for (int i = 0; i < quantity; i++)
                    {

                        var orderClone = (EbayOrderEntity)item.Clone();


						foreach (dynamic transaction in orderClone.order.TransactionArray)
						{
							transaction.QuantityPurchased = 1;

							//double sellPricePerItemQty = transaction.TransactionPrice.Value;

							////orderClone.order.Total.Value = sellPricePerItemQty;
						}
						
                        totalOrders.Add(orderClone);
                    }
                }
                catch { }
            }

            return totalOrders;
        }


        public List<EbayOrderEntity> SplitMultiOrders(List<EbayOrderEntity> multiorders)
        {
            List<EbayOrderEntity> totalOrdersTemp = new List<EbayOrderEntity>();

            foreach (dynamic multiOrder in multiorders)
            {
                int count = multiOrder.order.TransactionArray.Count();

                EbayOrderEntity ebayOrder = multiOrder.Order;
                for (int i = 0; i < count; i++)
                {

                    EbayOrderEntity ebayOrderTemplate = (EbayOrderEntity)multiOrder.order.Clone();

                    ebayOrderTemplate.order.TransactionArray = multiOrder.order.TransactionArray[i];

                    totalOrdersTemp.Add(ebayOrderTemplate);
                }
            }


            return totalOrdersTemp;
        }
    }
}
