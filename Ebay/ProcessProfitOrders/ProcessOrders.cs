﻿using DN_Classes.AppStatus;
using DN_Classes.Conditions;
using DN_Classes.Ebay.Category;
using DN_Classes.Entities.ProcessProfitOrder;
using DN_Classes.Products;
using DN_Classes.Products.Price;
using EbayOrderDB;
using MongoDB.Bson;
using PriceCalculation;
using PriceCalculation.Shipping;
using ProcessProfitOfOrderDll;
using ProcessProfitOfOrderDll.Entities;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WhereToBuy;


namespace ProcessProfitOrders
{
    public class ProcessOrders
    {
		//List<EbayCalculatedEntityV2> allCalculatedOrdersV2 = new List<EbayCalculatedEntityV2>();
        public Dictionary<EbayOrderEntity, EbayCalculatedProfit> ProcessAllItems(List<EbayOrderEntity> ebayOrder)
        {
            Dictionary<EbayOrderEntity, EbayCalculatedProfit> allCalculatedOrders = new Dictionary<EbayOrderEntity, EbayCalculatedProfit>();
			using (ApplicationStatus appStatus = new ApplicationStatus("ProcessProfit_Ebay_148"))
			{
				int count = 0;
				lock(new object()){
					ParallelOptions po = new ParallelOptions();
					po.MaxDegreeOfParallelism = 4;

					Parallel.ForEach(ebayOrder, po, singleItem =>
					{
						count++;
						Console.WriteLine("Processing: " + singleItem.order.OrderID + "\t" + count);
						string strMarketPlatform = "Ebay";
						var ExtractedFields = new GetFields(singleItem.order);
						string title = ExtractedFields.getTitle();
						string sellerSKU = getEan(title, ExtractedFields.getSKU());
						string itemOrderID = ExtractedFields.getOrderIdByItem();
						double itemPrice = ExtractedFields.getItemPerPrice();
						

						List<string> category = new GetCategory().GetCategoryByOrderId(itemOrderID);

						//double priceToPayFromEbay = ebayFeeFinder.GetFee(category, singleItem.order.Total.Value);
						FeeRule feeRule = new EbayFeeFinder().GetPrimaryFeeRule(category);

						ConditionSet conditionSet = new CategoryConditions().GetConditionSet("", strMarketPlatform, feeRule);
						
						string agentName = new GetAgentName().GetAgentNameByOrder((singleItem.plus["AgentID"].AsInt32).ToString());
						//shipping cost. ebay fee, payment fee. 

						try
						{
							string strEan = new Regex(@"\d{13}").Match(sellerSKU).ToString();
							var cheapestItem = new CheapestPrice().GetCheapestPriceByEan(strEan, "");
							bool fixPrice = cheapestItem.fixedPrice == "04" ? true : false;

							#region Product Measurements

							ProductMeasurements productMeasurements = new ProductMeasurements(new ProductSizeDB(),
																						strEan,
																						cheapestItem);

							ProductSize productExtractedMeasurements = productMeasurements.getProductMeasurements();

							BsonDocument plus = new BsonDocument();
							plus["FakeSizes"] = productMeasurements.ModifyIfFakeSizes();
							#endregion

							#region Shipping Cost Variation
							var shippingCostVariation = new ShippingCostFinder(productExtractedMeasurements,
																					itemPrice,//singleItem.order.Total.Value,
																					fixPrice)
																					.getShippingVariation();

							#endregion

							#region
							//singleItem.SalesChannel.ToUpper() != "AMAZON.DE" 

							//double tempShippingCost = ((singleItem.order.ShippingAddress.CountryName.ToUpper() != "DE") ||
							//						(singleItem.order.ShippingAddress.CountryName.ToUpper() != "DEUTSCHLAND"))
							//						? shippingCostVariation.price : 9.00;
							//double servicecost = singleItem.order.ShippingServiceSelected.ShippingServiceCost.Value;
							//double numberItems = servicecost / 2.9;
							//double tax = servicecost - (servicecost / 1.19);
							//double ebayfee = servicecost * (feeRule.Percentage/100);
							//	double tempShippingIncome = servicecost;//+ tax + ebayfee;
							//;/singleItem.order.ShippingServiceSelected.ShippingServiceCost.Value;
							#endregion



							double tempShippingIncome = string.Equals(agentName, "Albert Minin", StringComparison.CurrentCultureIgnoreCase) ? 2.9 : 0;
							//singleItem.order.ShippingServiceSelected.ShippingServiceCost.Value;


							#region Shipping
							Shipping shipping = new Shipping(shippingCostVariation.name,
																 shippingCostVariation.price,
																 conditionSet.Profit.ItemHandlingCosts,
																 productExtractedMeasurements.packing.netPrice);
							#endregion

							#region Set all required Entity for Product



							var product = new Product(sellerSKU,
															itemPrice,//singleItem.order.Total.Value,
															conditionSet,
															cheapestItem,
															productExtractedMeasurements,
															shippingCostVariation.price);


							#endregion

							double overAllProfit = product.getProfit(itemPrice, tempShippingIncome);

							#region Set Calculated Profit Entity

							EbayCalculatedProfit calculatedProfitEntity = new EbayCalculatedProfit(agentName,
																							cheapestItem,
																							singleItem.order,
																							category.First(),
																							productExtractedMeasurements,
																							conditionSet,
																							shipping,
																							overAllProfit
																							, plus,
																							tempShippingIncome,
																							itemPrice);


							#endregion



							allCalculatedOrders.Add(singleItem, calculatedProfitEntity);


						}
						catch (Exception ex)
						{
							if (ex is NoProductFoundException || ex is ItemNotAvailableException)
							{
								#region Set Default "Not Available" values

								Shipping shipping = new Shipping("not available", 0, 0, 0);
								BuyPriceRow emptyBuyPriceRow = new BuyPriceRow(sellerSKU);
								EbayCalculatedProfit calculatedProfit = new EbayCalculatedProfit(agentName, emptyBuyPriceRow, singleItem.order, category.First(), new ProductSize(), conditionSet, shipping, -3.33, new BsonDocument(), 0, 0);

								new InsertAndUpdateToDatabase().updateItemForNotavailableOrderForEbay(singleItem);
								#endregion


								allCalculatedOrders.Add(singleItem, calculatedProfit);
							}
						}
					});
				}


				//foreach (var singleItem in ebayOrder)
				//{
					
				//}


				appStatus.Successful();
			}
			//202831821019-
            return allCalculatedOrders;
        }

		private string getEan(string title, string sellerSKU)
		{
			string ean = "";

			var RegEx = new Regex(@"\d{13}");//.Match(sellerSKU).ToString();
			//var Match = RegEx.IsMatch(sellerSKU);

			if (RegEx.IsMatch(sellerSKU))
			{
				ean = RegEx.Match(sellerSKU).ToString();
			}
			else if (RegEx.IsMatch(title))
			{
				ean = RegEx.Match(title).ToString();
			}


			return ean;
		}
    }
}
